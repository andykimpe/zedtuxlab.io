## My career

Self-taught in programming in general, I am more oriented towards web development through my professional activity, I continue to develop other things in a completely different field.

My professional career begins, in the "region parisienne" in France, where I work for 2 years in an IT department at [Amec SPIE](https://www.spie.com/), to prepare Windows machines and perform network maintenance.

Thereafter, I move to the East of France and I will work in Luxembourg from 2006, as a developer at [KNEIP](https://www.kneip.com/), a company in the financial sector.
I will work there for ten years where I will evolve as much in programming languages as in my roles.
At KNEIP I worked there mainly with Ruby On Rails.

I leave KNEIP to go to work at [Yellow.lu](https://www.yellow.lu) ([Linc SA](https://www.linc.lu/)) for 1 year before returning to do an additonnal year at KNEIP.

It's in 2017 that I definitely left KNEIP to go to work for [Pharmony SA](https://www.pharmony.eu) where I worked with Ruby On Rails, React, Bootstrap (Commis), but also React-Native (Closed source Companion mobile application).

I'm now running my own business, since August 2020, as a remote developer from where I moved to.

My career an be viewed in detail on [my Linkedin page](https://www.linkedin.com/in/guillaumehain).

## Projects

Here is a non-exhaustive list of public projects that I have carried out:

 * [Brewformulas.org](https://gitlab.com/zedtux/brewformulas.org) - List of Homebrew formulas with a description automatically retrieved from the formula's official website.
 * [Douane](https://douaneapp.com/) - Software firewall for Linux (block traffic for a specific application).
 * [Commis](https://gitlab.com/pharmony/commis) - An orchestration tool for Chef / Knife / Knife-zero.

You can find all my public repositories on [Gitlab](https://gitlab.com/users/zedtux/projects).

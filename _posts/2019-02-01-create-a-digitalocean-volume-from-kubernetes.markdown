---
layout: post
title: Create a DigitalOcean volume from Kubernetes
date: '2019-02-01 05:19:43'
tags:
- docker
- kubernetes
---

In the case you're using the [DigitalOcean Kubernetes](https://www.digitalocean.com/products/kubernetes/), you can create [DigitalOcean Volumes](https://www.digitalocean.com/products/block-storage/) (no for free) with this simple YAML :

<script src="https://gist.github.com/zedtux/27d445f57fad3491c9d0398306db165c.js"></script>
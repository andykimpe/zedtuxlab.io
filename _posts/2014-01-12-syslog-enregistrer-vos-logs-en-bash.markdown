---
layout: post
title: 'syslog: Enregistrer vos logs en bash'
date: '2014-01-12 16:57:53'
tags:
- linux
- command-line
- bash
---

Pour créer des entrés log dans syslog depuis bash il suffit d’utiliser la commande `logger`.

Les paramètres sont simple, mais voici une toute petite description:

L’argument __-p__ permet définir la priorité, mais surtout où écrire les lignes de log.
Vous ne pouvez pas, cependant, créer une destination de votre choix. Vous n’avez le chois qu’entre auth, daemon, mail etc … ( voire man logger ).

L’argument __-t__ sera sûrement très utile: Il permet de tagger votre log. Généralement, il y ai mis le nom du process qui génère ce log.

Au final, voici un exemple :

    logger -p ‘user.info’ -t myexample Hello World
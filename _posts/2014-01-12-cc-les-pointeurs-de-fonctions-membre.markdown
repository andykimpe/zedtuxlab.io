---
layout: post
title: "[C/C++] Les Pointeurs De Fonctions Membre"
date: '2014-01-12 01:45:13'
---

En C, il existe les pointeurs.
Asser complexe à comprendre, mais très utile. Ces pointeurs s’appliquent beaucoup sur les variables, mais peuvent aussi être utilisé pour les fonctions.

En C++, les pointeurs sont remplacé par des Références, en ce qui concerne les variables.

Mais tout deux ont le point commun de pouvoir utliser des pointeurs de fonctions.

J’ai trouvé une documentation particulièrement bien écrite, en Francais, au format PDF : http://www.newty.de/fpt/zip/f_fpt.pdf
La version Anglaise est aussi disponible sur le site : http://www.newty.de/fpt/index.html

Si vous n’êtes pas trop à l’aise avec les pointeurs de fonctions… ou même si vous l’êtes, lisez ceci, et croyez moi, il n’y aura plus trop de flou sur les pointeurs de fonctions ! :) 
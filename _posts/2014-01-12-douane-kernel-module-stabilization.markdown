---
layout: post
title: 'Douane: Kernel module stabilization'
date: '2014-01-12 20:35:11'
tags:
- my-developments
- douane
---

Some days ago I’ve [announced my brand new project](/announcing-douane/) **Douane**.

# Current state

Daemon and [Linux Kernel module](http://en.wikipedia.org/wiki/Kernel_module) communication is now fixed and work pretty well, but my development environment (a [VirtualBox](https://www.virtualbox.org/) instance hosting **Ubuntu Precise 12.04**) is crashing after few minutes.

So I’m now working on the stabilization of the kernel module.

# Something to show us?

Still not. Like in my previous post, sorry.
---
layout: post
title: Devise Google Authenticator with Cucumber
date: '2018-03-12 07:38:42'
tags:
- rails
- cucumber
---

In a project I am working on, the [devise_google_authenticator](https://rubygems.org/gems/devise_google_authenticator) gem is used in order to improve the login security.

I'm working on writing automated tests using Cucumber and I had to deal with it.

I have written a composed Cucumber step which:

1. Creates a user
2. Login the user
3. Select the Google Authenticator mode (we have 2 different options)
4. Ends on the page where to register the token and fill in the [OTP](https://en.wikipedia.org/wiki/One-time_password).

I found a very simple way to handle this and get a valid OPT with Cucumber. I found the piece of code calculating valid OTPs, from the devise_google_authenticator gem, and build a small Cucumbler helper.

Create the new file `features/support/google_authenticator.rb` with the following content:

<script src="https://gist.github.com/zedtux/c0009c6da743c6082062aaec4b0cafa6.js"></script>

Now from my step I'm using `fill_in 'user_gauth_token', with: a_valid_otp_for(user)` and it's working.
---
layout: post
title: Use your DigitalOcean Kubernetes cluster with Gitlab CI
date: '2019-02-12 10:36:11'
tags:
- docker
- kubernetes
---

In this short article I'm explaining how to integrate an existing DigitalOcean Kubernetes cluster with Gitlab CI.

Gitlab team has implemented the awesome [Auto DevOps feature](https://docs.gitlab.com/ee/topics/autodevops/) which leverage Kubernetes in order to run your builds, mount isolated environment per branches and deploy your app.

But filling the Gitlab form to setup the integration is not that easy when you got a YAML file from DO.

So let's demysify it one for all 😇.

## The form

Here is the integration form from Gitlab :

![gitlab-kubernetes-integration-form](/content/images/2019/02/gitlab-kubernetes-integration-form.png)

And the Gitlab documentation explaining how to [connect Gitlab with a Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/#adding-an-existing-kubernetes-cluster).

## The DO file

Here is a sample file with redacted data 😁 (output of the `kubectl config view`) :

```
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://<UUID>.k8s.ondigitalocean.com
  name: do-fra1-dev-k8s-3gb-1vcpu
contexts:
- context:
    cluster: do-fra1-dev-k8s-3gb-1vcpu
    user: do-fra1-dev-k8s-3gb-1vcpu-admin
  name: do-fra1-dev-k8s-3gb-1vcpu
current-context: do-fra1-dev-k8s-3gb-1vcpu
kind: Config
preferences: {}
users:
- name: do-fra1-dev-k8s-3gb-1vcpu-admin
  user:
    client-certificate-data: REDACTED
    client-key-data: REDACTED
```

## Filling the form

### Kubernetes cluster name

Here I'm using the same name as the cluster, so `dev-k8s`, but you can put whatever make sens to you.

### API URL

Copy / past the `clusters.cluster.server` content which is like `https://<UUID>.k8s.ondigitalocean.com`.

### CA Certificate

Here comes the reason of this article 😅.

From the DO file, you have it in the `clusters.cluster.certificate-authority-data` but as a base64 encoded string, while Gitlab expect it has a non encoded string.

It should be composed like that :

```
-----BEGIN CERTIFICATE-----
MIIDJzCCAg+gAwIBAgICBnUwDQYJKoZIhvcNAQELBQAwMzEVMBMGA1UEChMMRGln
aXRhbE9jZWFuMRowGAYDVQQDExFrOHNhYXMgQ2x1c3RlciBDQTAeFw0xOTAxMDMx
.....
6WfbUa1Tq/BElIh1rXcNBQ6XqJ1r4OlTUFQqs2HyppEvgDlLgC4GInTdHA==
-----END CERTIFICATE-----
```

To decode the string, on a Mac, you can do like that :

```
$ echo "LS0tLS1CRUdJTiBDR...LS0tLS0K" | base64 -D
```

On Linux is the same but you replace the `-D` flag by `--decode` as explain in [this SO answer](https://askubuntu.com/a/178546).

### Token

After having followed the instruction from the documentation I gave form the **The Form** paragraph, you'll have a `gitlab` ServiceAccount and a Secret which holds the token.

As for the certificate, we got a base64 encoded string that we have to decode before to copy/past it in the Gitlab form.

Here is a one-command in order to fetch the token :

```
$ kubectl get secret $(kubectl get sa gitlab --output jsonpath='{.secrets[0].name}') --output jsonpath='{.data.token}' | base64 -D
```
_This selects the gitlab ServiceAccount, fetches the name of the first secret, then selects the Secret by its name and fetches its token._

### Project namespace

I'm leaving it empty, but you can also put whatever make sens to you until it is unique.

## Conclusion

You should be now good and installing Tiller should just go fine as for the Gitlab runner.
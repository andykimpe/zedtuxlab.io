---
layout: post
title: 'C++: Les fonctions “inline”'
date: '2014-01-12 17:23:41'
tags:
- c-2
---

Le mot clé `inline`, qui se place devant la déclaration d’une fonction dans la déclaration de la classe (dans un fichier header **.h** en règles générales), permet d’optimiser le code, d’accélérer le fonctionnement de la classe.

La particularité est qu’il ne peux être utilisé que si la méthode, déclaré avec ce dernier, ne modifie pas la valeur d’un attribue de la classe.

En clair, les getters !

Donc, il est bon de déclarer tout les `get…()` avec `inline`, si il n’y a pas de changement de valeurs :

<script src="https://gist.github.com/zedtux/8387676.js"></script>

**Le C/C++ c’est bon ! Mangez-en !!**
---
layout: post
title: Rails + Devise working with multiple models
date: '2015-01-08 19:49:43'
tags:
- rails
- devise
---

The usual case with devise is to have users. In this case Devise is really easy: 1 command to run and you're done.

But when you want to manage different kind of users like having a class `User` and a class `Admin`.
Some people prefer to not create a second model (maybe due to the difficulty to manage both ?) and use technics like using [Concern](http://api.rubyonrails.org/classes/ActiveSupport/Concern.html) and so on.

Personnaly I want the 2 models.

### Something changed

Since the 13th of August 2014 a new helper has been released in Devise:

    3.3.0 - 2014-08-13
     - enhancements 
     	...
        - Adds devise_group, a macro to define controller helpers for multiple mappings at once. (by @dropletzz)
        ...

[Dropletzz](https://github.com/dropletzz) has implemented an helper method which allows you to define a group of mappings which generates some methods like explained [in the documentation](http://www.rubydoc.info/github/plataformatec/devise/master/Devise/Controllers/Helpers/ClassMethods:devise_group).


### How to use devise_group

In my case I need this group all the time so I'm adding this to the `ApplicationController`:

```ruby
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  devise_group :person, contains: [:user, :admin]
  before_action :authenticate_person! # Ensure someone is logged in
end
```

Now I can use everywhere in my application `current_person` where the code is common to the `User` and `Admin` classes.
I can still use `current_user` and `current_admin` which is cool.

### Thank you

I would like to thank Dropletzz :) and you for reading this.
---
layout: post
title: 'Machinarium: Un autre jeu sans DRM et portable !'
date: '2014-01-12 18:21:16'
tags:
- linux
- game
---

![Machinarium](/content/images/2017/08/Machinarium_PC_jaquette.jpg)

Machinarium est un jeu fonctionnant sous GNU/Linux, MAC OSX et Windows.
Mais sa grande particularité est qu’il est sans DRM !

## Sans DRM ?? Ca veux dire quoi ?

Ca veux tout simplement dire, qu’a l’inverse des jeux commerciaux habituel, avec leur code que de toutes manière sera cassé un moment ou l’autre, ce jeu lui s’installe et se joue ! C’est tout !

Pas de “Veuillez insérer le CD de XXXXXXX” !

Bref, jouer tranquillement.

Il est important de supporter ces jeux sans DRM, surtout vue leur prix dérisoire de 15 à 20€ !

Cette initiative est très courageuse, et odasieuse ! Surtout de nos jours, où nous vivons dans un monde capitaliste qui n’as qu’un mot d’ordre “Faire toujours plus de frique !”

## Et le jeu en lui même ?

Voici la deuxième partie intéressante de ce jeu: Ce qu’il est !

Le gameplay est un simple Point&Click. C’est à dire, vous avez des environnements, vous déplacez votre personnage, puis vous pouvez interagir avec le décore, et résoudre des énigmes !

Les graphismes sont magnifiques, et très originaux !

Les énigmes à résoudre sont très bien ficelé ! Il existe un livre qui explique comment passer le niveau, mais ce bouquin est protégé par un petit jeu, qui permettra son ouverture qu’une fois le mini jeu terminé ! Très ingénieux !

Les musiques sont d’une qualité splandide, et leur thème tout à fais adapté au jeu, à l’environnement, à l’ambiance !

De plus, elle sont douce, et permet donc d’être bien détendu !

## Comment supporter les développeurs ?

Il vous suffit de vous en acheter une copie, qui avoisine les 15-16€, autant dire, presque rien, et vous aurez accès au téléchargement :

 - de la version Windows
 - de la version Mac
 - de la version GNU/Linux
 - des soundtrack

Cet achat est intelligent, sans trop de conséquences dans votre porte monaie, ou votre carte de payement plustot, et vous passerez un très bon moment !

Le lien pour acheter le jeu: http://machinarium.com/order.html


## Peux-t-on le tester ?

Oui, en première page, une version flash de la démo est accessible !

Leur site est http://machinarium.com/

Vous pourrez l’acheter à ce même endroit !
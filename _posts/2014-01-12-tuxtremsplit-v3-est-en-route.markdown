---
layout: post
title: TuXtremSplit v3 est en route !
date: '2014-01-12 20:00:00'
tags:
- mes-developpements
- my-developments
- python
- tuxtremsplit
---

![python](/content/images/2017/08/python.png)

Vous devez vous dire “Encore une nouvelle version alors que la 2.x n’est pas fini ??” :)

En effet, j’ai commencé la fin de semaine dernière la ré-écriture de TuXtremSplit[[sur Launchpad](http://launchpad.net/tuxtremsplit)], mon clone d’[Xtremsplit](http://xtremsplit.fr/) !

## Pourquoi une nouvelle version ?

Pour ceux qui ont suivi [le fil de discussion du forum ubuntu-fr.org](http://forum.ubuntu-fr.org/viewtopic.php?id=34185), ou qui me parle par messagerie instantané, vous savez que ce projet est un projet d’apprentissage pour moi.

C’est à dire que j’apprends les langages de programmation grâce à ce projet.
La version 1 était en C, la version 2 en C++ et maintenant la version 3 en Python.

Ce qui explique pourquoi le projet évolue aussi bizarrement.

## Pourquoi Python ?

La première raison est que Python est très populaire sur ma distribution préféré: Ubuntu.
Plusieurs sessions sur IRC à suivre des présentations de Python et ton son petit monde, des tas d’applications en Python etc …

La second raison est que actuellement, au boulot, je bosse avec du Ruby, qui est un langage très ressemblant au Python.
(Cela dis, je préfère Ruby à Python.)

## Ce qui est prévue au programme ?

Une fois tout le code écrit pour réaliser les deux taches principale du programme, qui sont joindre et couper des fichiers, je vais forcer l’accent sur l’intégration dans GNOME.

 - J’ai très récemment lu le GNOME Human Interface Guidelines, que je vais essayer de suivre au maximum.
 - J’imagine une intégration tel, qui vous permettent, en un double clique sur un fichier xxxxxxxxxxx.001.xtm de lancer le collage des fichiers.
 - Je vais aussi reprendre là où j’en étais avec l’intégration dans Nautilus (Icône spécial pour le fichier 001.xtm)

Bref, j’espère produire un outil peaufiné dans ses moindre détails !
En espérant aussi avoir du retour de votre part sur ce qui manquerai, serai à changer, etc … :D 
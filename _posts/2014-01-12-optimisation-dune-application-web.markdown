---
layout: post
title: Optimisation d’une application web
date: '2014-01-12 12:39:05'
tags:
- optimisation
- apache
- php
- memcached
---

Par défaut, les applications qui permettent de faire tourner votre serveur LAMP font tourner vos sites wouaibe facilement.
Mais, coté performances, il faut traiter chacun son propre cas.
C’est à dire, que selon votre mémoire, votre CPU, etc… vous devez adapter les paramètres.

Mais il existe quelques optimisations, qui n’en dépendent pas, et qui vous permettrons d’améliorer la rapidité de réponse de vos sites.

Coté MySQL, il est apparemment utile de désactiver InnoDB… Personnellement je ne l’ai pas fais, car je ne sais pas ce que c’est, et j’ai pas le temps, là tout de suite, de m’y intéresser.

Par-contre, coté PHP, il y a 2 choses faisables :

 - Augmenter la taille maximal de la mémoire utilisée entre PHP et Apache.
 - Utiliser memcached, pour gérer d’une autre manière la mémoire.

Le premier point se faire très simplement:

    sudo nano /etc/php5/apache2/php.ini

Puis, rechercher (CTRL+W) `memory_limit` et changer la valeur par défaut (16) en 64.

Le second point consiste juste en l’installation de memcached et d’un module pour php pour qu’il utilise ce dernier:

    sudo apt-get install php5-memcache memcached

Maintenant, il ne reste plus qu’a redémarrer apache:

    sudo /etc/init.d/apache2 restart

Vous devriez voir une amélioration.

Source: http://blog.bodhizazen.net/linux/optimize-wordpress-for-speed/
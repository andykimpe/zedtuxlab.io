---
layout: post
title: Douane version 0.2.0 disponible
date: '2014-01-13 12:58:04'
tags:
- mes-developpements
- douane
---

Après les 2 semaines qui ont suivi la sortir de la toute première version de test de Douane, voici la version 0.2.0 disponible.

De la première version a la version 0.2.0 il n’y a eut principalement des correctifs due a l’ajout du fichier `.desktop` afin de lancer plus facilement le configurator (qui vous permet de trouver un lanceur dans Unity et GNOME shell).

La seule amélioration (qui a été proposé par [hotice](https://github.com/hotice)) est l’utilisation de [pkexec](http://www.freedesktop.org/software/polkit/docs/0.105/pkexec.1.html) a la place de gksu.

La version 0.2.0 est disponible pour Ubuntu **precise** et **raring**: https://github.com/zedtux/Douane/tree/master/packages
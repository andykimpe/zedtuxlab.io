---
layout: post
title: 'Yml2Sql: Convertir une fixture yml en fichier sql'
date: '2014-01-12 19:51:04'
tags:
- ruby
---

Je devais utiliser une fixture au format [YAML](http://www.yaml.org/)[[Wikipedia](http://fr.wikipedia.org/wiki/YAML)] (.yml) pour peupler ma db PostgreSql[Wikipedia] mais apparemment c’est pas trop faisable directement avec rake ou autres.

Google ne me satisfaisant pas .. j’ai écrit un script [ruby](http://www.ruby-lang.org/fr)[[Wikipedia](http://fr.wikipedia.org/wiki/Ruby)] (à l’arrache !) qui convertis un fichier YML en SQL qui puisse être utilisé par `psql` pour importer les données.

Le voici :

<script src="https://gist.github.com/zedtux/8389527.js"></script>
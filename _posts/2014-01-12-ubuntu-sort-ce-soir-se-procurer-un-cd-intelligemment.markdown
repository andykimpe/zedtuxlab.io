---
layout: post
title: 'Ubuntu sort ce soir: Se procurer un CD intelligemment !'
date: '2014-01-12 18:26:26'
tags:
- linux
- ubuntu
---

![download_iso_with_transmission](/content/images/2017/08/download_iso_with_transmission.png)

Ceci n’est pas une charade ! :D

Voici un petit article pour promouvoir l’utilisation du torrent pour télécharger l’iso d’une image d’une distribution, à l’aube de la sortie d’**Ubuntu Karmic Koala** !

## Le problème

Le problème est que tout le monde fonce télécharger sa copie, et quoi de plus normal, mais directement sur les serveurs, qui se retrouvent vite, submergé par les demandes.

Du coup, tout le monde se gène, et nous nous ralentissons les uns les autres.

## La solution

La solution à ce problème, est d’utiliser l’outil à notre disposition, qui est tout à fais adapté à la situation, pour que tout le monde puisse aller vite, sans gêner les autres, mais au contraire, en aidant les autres, c’est à dire, en leur permettant d’aller plus vite !

Pour rester dans l’optique de la **communauté**, de **partage**, il faut donc utiliser [torrent](http://fr.wikipedia.org/wiki/BitTorrent_%28protocole%29) pour [télécharger Karmic Koala](http://www.ubuntu-fr.org/telechargement), une fois qu’il sera sortis, et donc, le client installé par défaut sous Ubuntu est Transmission.

Il faut donc privilégier le téléchargement par torrent, afin de télécharger soit même rapidement, et de permettre aux autres de télécharger rapidement.
---
layout: post
title: 'Ubuntu 10.04: Nautilus intégre pleinement les dossiers partagés !'
date: '2014-01-12 19:32:07'
tags:
- linux
- gnome
---

![120px_Nautilus_icon_svg_](/content/images/2017/08/120px_Nautilus_icon_svg_.png)

Je vient de découvrir vite fais ce soir, sur mon petit **Ubuntu 10.04 Lucid Lynx** que [nautilus](http://live.gnome.org/Nautilus) intégre les dossiers partagé pleinement !

En effet, comme il le fait pour les dossiers contenant des photos ou de la musique, il affiche un bandeau avec un petit bouton pour accéder rapidement et simplement aux paramètres de partages !

Et voici la capture :

![nautilus_shared_folders](/content/images/2017/08/nautilus_shared_folders.png)


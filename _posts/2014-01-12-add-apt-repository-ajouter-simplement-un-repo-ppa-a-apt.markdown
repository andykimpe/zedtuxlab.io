---
layout: post
title: 'add-apt-repository: Ajouter simplement un repo PPA à APT !'
date: '2014-01-12 18:34:25'
tags:
- ubuntu
- apt
---

![Ubuntu Debian package](/content/images/2017/08/deb-1.png)

Je vient tout juste de découvrir cette magnifique commande, donc je partage ! :-)

## add-apt-repository

Pas la peine d’écrire des kilomètres d’article !
Cette commande permet d’ajouter un dépôt [PPA (Personal Package Archive)](https://help.launchpad.net/Packaging/PPA?action=show&redirect=PPAQuickStart) au fichier source.list d’[apt (le gestionnaire de paquet deb)](http://fr.wikipedia.org/wiki/Advanced_Packaging_Tool) !
Elle est disponible depuis la version [9.10 (Karmic Koala)](http://doc.ubuntu-fr.org/karmic).

C’est à dire qu’il ajoute les célèbre lignes `deb http://ppa.launchpad.net/…` mais il télécharge et installe aussi les certificats GPG !

Elle est pas belle la vie !? 8-)

## Petit exemple ?

Ok, voici un petit exemple !

Je désire installer les drivers Nvidia patché avec vdpau (pour utiliser le GPU plus tot que le CPU pour décoder les vidéo HD) :

    sudo add-apt-repository ppa:nvidia-vdpau
    [sudo] password for zedtux:
    Executing: gpg –ignore-time-conflict –no-options –no-default-keyring –secret-keyring /etc/apt/secring.gpg –trustdb-name /etc/apt/trustdb.gpg –keyring /etc/apt/trusted.gpg –keyserver keyserver.ubuntu.com –recv 71609D4D2F1518FA9C5DC0FB1DABDBB4CEC06767
    gpg: requête de la clé CEC06767 du serveur hkp keyserver.ubuntu.com
    gpg: clé CEC06767: clé publique « Launchpad Nvidia Vdpau Team PPA » importée
    gpg: Quantité totale traitée: 1
    gpg: importée: 1 (RSA: 1)

Ensuite, il ne reste plus qu’à faire l’habituel

    sudo apt-get update

Et les nouveaux paquets sont prêt à être installé !

**Ubuntu…. c’est trop fort !**
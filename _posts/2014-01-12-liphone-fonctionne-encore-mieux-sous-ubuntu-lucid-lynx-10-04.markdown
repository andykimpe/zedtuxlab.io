---
layout: post
title: L’iPhone fonctionne encore mieux sous Ubuntu Lucid Lynx 10.04 !
date: '2014-01-12 19:37:28'
tags:
- linux
---

![ubuntu_10_04_iphone_v2](/content/images/2017/08/ubuntu_10_04_iphone_v2.png)

Nouveau billet sur l’iPhone sous Ubuntu Lucid Lynx 10.04 !

Si sur la capture d’écran vous ne voyez que l’icône du téléphone et pas celle de l’appareil photo numérique comme dans mon poste précédent [L’iPhone fonctionne mieux sous Ubuntu Lucid Lynx 10.04 !](/l’iphone-fonctionne-mieux-sous-ubuntu-lucid-lynx-10-04/) c’est tout à fait normal car ce dernier à disparut, ce qui fait du sens !

## Fuse corrigé !

Je n’ai pas eut de problèmes sur mon gros Pc fix, comme sur mon Acer Aspire One.

**iPhone branché = iPhone qui fonctionne !** Et ca rime en plus !!

## Autre point important

J’ai désinstallé tout ce qui est et appartient à Mono, puis j’ai vérifier que l’iPhone fonctionne toujours car dans le lot des paquets désinstallé, il y en avait un qui s’appelle `podsleuth` et qui joue une rôle dans la détection d’appareil de la firme à la pomme croqué !

Mais lorsque j’ai branché l’iPhone, il a été détecté, reconnu est accessible par Rhythmbox, et les synchronisations aussi bref tout marche !! Ouf ! :p
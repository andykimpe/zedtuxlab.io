---
layout: post
title: 'Linux On Mac: Smart zoom in Iceweasel'
date: '2015-05-16 11:34:41'
tags:
- linux-on-mac
---

A feature I loved a so much on OS X is the Safari smart zoom (like double tap an area in Safari on an iphone/ipad). I do not like reading blog articles or any kind of texts with a small font size. Instead I prefer to read in full screen (27' iMac).

I would love that this feature is supported by the graphical server ([X11](http://en.wikipedia.org/wiki/X_Window_System)) (maybe [Wayland](http://en.wikipedia.org/wiki/Wayland_%28display_server_protocol%29) would support it ?) so that it could be used outside of the web browser like when opening an image or a PDF.

[Iceweasel](https://wiki.debian.org/Iceweasel) is a Debian version of Firefox which mainly change the name and the logo (with some Debian patches) which means that the Firefox add-on work.

There is one which do exactly what I want and it's [Zoomr](https://addons.mozilla.org/en-Us/firefox/addon/zoomr/). Install it and then hold the click on the area where to zoom. : )
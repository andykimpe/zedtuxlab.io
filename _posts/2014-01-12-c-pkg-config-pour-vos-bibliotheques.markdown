---
layout: post
title: 'C++: pkg-config pour vos bibliothèques !'
date: '2014-01-12 17:35:19'
tags:
- c-2
---

## C’est quoi pkg-config ?

Ce programme (Linux, mais dispo pour Windows et Mac), permet de simplifier la compilation d’application, utilisant des bibliothèques partagé sur le système.

En gros, vous lui donnez le nom de la bibliothèque visé, et il peut vous retourner le opérateurs de compilation, et les Includes path.

## Un exemple

Imaginez vouloir compiler un programme qui utiliser Gtkmm-2.4

    $ pkg-config --libs --cflags gtkmm-2.4
    -D_REENTRANT -I/usr/include/gtkmm-2.4 -I/usr/lib/gtkmm-2.4/include -I/usr/include/glibmm-2.4 -I/usr/lib/glibmm-2.4/include -I/usr/include/giomm-2.4 -I/usr/lib/giomm-2.4/include -I/usr/include/gdkmm-2.4 -I/usr/lib/gdkmm-2.4/include -I/usr/include/pangomm-1.4 -I/usr/include/atkmm-1.6 -I/usr/include/gtk-2.0 -I/usr/include/sigc++-2.0 -I/usr/lib/sigc++-2.0/include -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -I/usr/lib/gtk-2.0/include -I/usr/include/cairomm-1.0 -I/usr/include/pango-1.0 -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/freetype2 -I/usr/include/directfb -I/usr/include/libpng12 -I/usr/include/atk-1.0 -lgtkmm-2.4 -lgiomm-2.4 -lgdkmm-2.4 -latkmm-1.6 -lgtk-x11-2.0 -lpangomm-1.4 -lcairomm-1.0 -lglibmm-2.4 -lsigc-2.0 -lgdk-x11-2.0 -latk-1.0 -lpangoft2-1.0 -lgdk_pixbuf-2.0 -lm -lpangocairo-1.0 -lgio-2.0 -lcairo -lpango-1.0 -lfreetype -lfontconfig -lgobject-2.0 -lgmodule-2.0 -lglib-2.0

Autant vous dire qu’il faudrait être fou pour retenir tout ce charabia !

## Les avantages

Bon, le premier, tout le monde l’as maintenant pigé je pense, il facilite la vie, surtout si vous avez plein d’opérateurs ou includes path (les “-I /path/to/header/files/”).

Un autre avantage est si vous utilisez **les auto tools** !

En enffet, vous pouvez spécifier les dépendances de la bibliothèque, et du coup, quand vous aller executer le configure, il va vérifier que tout le système comporte tout ce qu’il faut pour que votre bibliothèque fonctionne.

Sinon, configure s’arrête, et le Makefile n’est pas généré, donc pas de make.


## Les bibliothèques disponible avec pkg-config

Malheureusement, toutes les bibliothèques ne sont pas disponible dans `pkg-config` !

Et ceux, car les développeurs de la bibliothèque n’as pas fait le nécessaire !

Pour savoir quels sont les bibliothèques utilisable via `pkg-config`, il suffit de faire:

    pkg-config --list-all

Vous allez avoir la liste des bibliothèques disponible et une description.

Donc il vous suffit d’utiliser son nom avec `pkg-config` (avec les opérateurs **--libs** et/ou **--cflags** selon votre besoin ) pour avoir tout les détails de compilation.

Pour avoir une librairie listé par pkg-config, il suffit de créer un fichier `.pc` dans le répertoire `/usr/lib/pkgconfig/`.


# Créer un fichier .pc pour sa bibliothèque

Le fichier n’as rien de bien extraordinaire.
Faisons un exemple.

Imaginons que vous avez créer une bibliothèque `libhelloworld.so`.

Alors il faudrait un fichier `/usr/lib/pkgconfig/libhelloworld.pc` avec :

    prefix=/usr
    exec_prefix=${prefix}
    libdir=${exec_prefix}/lib
    includedir=${prefix}/include

    Name: HelloWorld
    Description: Hello World library, written by myself
    Requires:
    Version: 1.0.0
    Libs: -L${libdir} -lhelloworld
    Cflags: -I${includedir}/helloworld

Donc, vous avez votre fichier `libhelloworld.so` dans `/usr/lib/` ainsi que les fichiers d’entête dans le dossier `/usr/include/helloworld/`.

Si vous aviez une dépendance pour votre bibliothèque, vous suffirai de l’ajouter à la ligne `Requires:`.

Par exemple, si vous aviez besoin de `libssl` :

    Requires: libssl

Résultat:

    $ pkg-config --clibs --cflags libhelloworld
	-I/usr/include/helloworld -lhelloworld

Magique ! :D

# Vérifier l’existence de ma bibliothèque avec les autotools

Alors maintenant que `pkg-config` reconnait la bibliothèque **libhelloworld**, vous allons demander au script configure de vérifier qu’elle est bien présente grâce à la macro `PKG_CHECK_MODULES`.

Donc, dans le fichier `configure.ac` de votre projet, il faut juste ajouter ceci :

    PKG_CHECK_MODULES([HelloWorld library], [libhelloworld >= 1.0.0])

Et `configure` fera :

    Checking for HelloWorld library ... Yes

# Utiliser pkg-config dans le Makefile du programme client

Il faut donc utiliser `pkg-config` dans le `Makefile.am` du projet utilisant votre bibliothèque afin de profiter de ce dernier.

`pkg-config` est à utiliser pour le compilateur et le linker.

Personnellement je déclare une variable que je réutilise dans les deux :

<script src="https://gist.github.com/zedtux/8387822.js"></script>

**Encore une fois ! C++ c’est bon !! Mangez-en !!** :-)
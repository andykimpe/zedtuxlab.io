---
layout: post
title: Lister les fichiers modifiés entre deux branches Git
date: '2015-02-05 10:52:38'
tags:
- git
---

Si comme moi vous avez besoin de la liste des fichiers qui ont été mis à jour entre deux branches git, voici la commande "magique":

    git diff --name-only release master

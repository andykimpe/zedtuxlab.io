---
layout: post
title: Monter un Serveur Proxy avec squid
date: '2014-01-12 17:58:13'
tags:
- server
---

![Squid logo](/content/images/2017/08/squid2.gif)

Il arrive que parfois, vous soyez sur une machine qui ne vous appartient pas, et qui donc ne vous permet pas de faire tout ce que vous désirez.

Quoi de plus normal ?

Alors comment faire ?

Utiliser un proxy !

# Principe du proxy

Pour ceux qui ne savent pas encore ce qu’est un serveur proxy, aussi appelé serveur mandataire, c’est pas difficile, c’est une machine qui sert de porte de sortie sur internet !

Donc, si mon navigateur web est paramétré avec un proxy, quand je vais sur un site web, le site verra la machine proxy, et pas ma machine.

# Avantages

Les avantages d’un serveur mandataire sont multiples.

Le premier avantage est de pouvoir naviguer anonymement sur la machine où vous êtes (Surtout si vous cryptez la communications entre vous et le proxy).

Un autre avantage est que vous profitez de la bande passante de votre serveur. C’est à dire que parfois, il arrive que l’endroit d’où vous vous connectez passe par un ensemble de serveurs/routeurs qui seront plus lents.

Encore un autre avantage est que le serveur proxy va créer un cache de vos navigations. Il est très probable que vous alliez régulièrement sur le(s) même(s) site(s).
Comme ils seront dans le cache du serveur mandataire, si il n’y a pas de mises à jour sur le site, les fichiers ne seront pas re-téléchargés.
Donc vous aurez un gain de performances.

# Installation

Comme notre distribution préférée est parfaite, elle permet de l’installer très simplement, et ne vous inquiétez pas, par défaut, squid n’autorise rien !

Donc vous ne risquez pas que l’on se serve de votre serveur pour faire des trucs pas bien :)

    sudo apt-get install squid

Whouaou ! Difficile hein ! ;) 

# Configuration de Squid

Attention… la difficulté augmente ! :-D

Pour modifier les paramètres, soit vous passez par webmin… ou vous modifiez en mode texte…

Perso, `nano` par `ssh` est mon amis ! ;)

Allez hop !

    sudo nano /etc/squid/squid.conf

Le fichier est gros… mais nous n’allons modifier que très peu de choses.

### Nom du serveur

Commençons par annoncer qui nous sommes !

Le paramètre à modifier est :

    visible_hostname nomduserveur

### Cacher l’IP du serveur

Pour ne pas avoir l’adresse IP de votre serveur dans les requêtes, il faut ceci :

    forwarded_for off

Que c’est dur !! :-D

### Messages d’erreur en français !

Pour ceux qui ne sont pas anglophones, avoir les messages d’erreurs en français peut être intéressant.
Pour cela, il faut juste changer la position où squid va chercher les messages :

    error_directory /usr/share/squid/errors/French

### Port de squid

Pour définir le port où squid doit écouter, il faut changer :

    http_port 1234

(Dois-je préciser qu’il faut ouvrir le port dans le firewall ? :-))

### Accès au cache

Pour que Squid puisse monter son cache (mettre les fichiers téléchargés quand vous allez sur un site, pour ne pas les re-télécharger si vous retournez dessus), il faut lui préciser le groupe à utiliser :

    cache_effective_group root

### Appliquer les droits

Pour que tout se passe bien, il faut changer les droits de deux chemins :

    sudo chown proxy:root -R /var/log/squid
    sudo chown proxy:root -R /var/run/samba/winbindd_privileged

(Si vous n’avez pas le dernier dossier, c’est pas un soucis, ignorez-le)

## Redémarrage

Pour prendre en compte toutes ces belles modifications, relançons notre proxy (qui ne permet toujours pas de naviguer ;-))

    /etc/init.d/squid restart

## Restreindre l’accès

Comme nous ne voulont pas passer pour des admin n00b, nous allons restreindre l’accès, histoire que notre proxy ne serve pas a autre chose.

Donc l’idée est trèèès simple… surtout si vous avez déjà fais du `.htaccess` avec Apache ! :-)

### Créer un fichier de mots de passe

Pour ceux qui connaissent Apache, ils ne seront pas dépaysés !

    sudo htpasswd -c /etc/squid/squid_passwd nomutilisateur

Puis on définit les droits de ce fichier :

    sudo chmod o+r /etc/squid/squid_passwd

### Activer la restriction d’accès

Maintenant, informons notre petit Squid qu’il ne faut pas laisser n’importe qui utiliser le proxy :

    sudo nano /etc/squid/squid.conf

Au début du fichier (après les 3 km de commentaires), vous trouverez les lignes à dé-commenter et/ou à modifier :

    auth_param basic program /usr/lib/squid/ncsa_auth /etc/squid/squid_passwd
    auth_param basic children 5
    auth_param basic realm Squid proxy-caching web server
    auth_param basic credentialsttl 2 hours
    auth_param basic casesensitive off

Explications :

 1. Définit l’emplacement du fichier de mots de passe à squid
 2. Nombre de processus d’authentification
 3. Message affiché dans la boîte de dialogue de demande du login / mot de passe
 4. Temps de validité d’une connexion avec le login / mot de passe
 5. Définit si le nom d’utilisateur est sensible à la casse ou pas. (majuscules / minuscules).

Puis on ajoute à la suite :

    acl ncsa_users proxy_auth REQUIRED
    http_access allow ncsa_users

On referme le fichier, puis on redémarre le serveur.

# Conclusion

Rien de bien difficile donc, quand on sait quoi changer.

Votre serveur vous permet donc maintenant un accès rapide, et anonyme sur le machine où vous vous situez.

De plus, si vous activez le mode anonyme de Firefox, vous serez tranquille !

Avant que l’on repère ce que vous avez fait, il faudra un admin réseau qui connaisse un peu, et dont le trip est de se taper du fichier de log.
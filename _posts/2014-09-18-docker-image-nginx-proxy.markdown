---
layout: post
title: Docker image nginx-proxy
date: '2014-09-18 14:19:31'
tags:
- docker
---

## Description

I have build a Docker image nginx-proxy which implement an Nginx revert proxy to your server.

Instead of configuring couple of times Nginx in the different images you have, why not do it automagically ? :-)

The original blog post where it is described is [here](http://jasonwilder.com/blog/2014/03/25/automated-nginx-reverse-proxy-for-docker/).

## HowTo

You have to start this image first so that it will bind to the port 80. Then any images where you need an HTTP access then you just have to start it with the environment variable `VIRTUAL_HOST` and it will work.

#### Start the reverse proxy

In a terminal, logged in to the server where Docker is running, execute the following:

    docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock zedtux/nginx-proxy 

As you can see here, we are binding the port 80 of the container to the port 80 of the server so that any requests coming to the port 80 are handeled by the containter.

Then we mount the docker socket to the container so that the container can detect new started container.

#### Start a web application and bind it to Nginx

Now is the tricky stuff.

In the terminal execute the following in order to start a [Ghost blogging](http://ghost.org) instance:

    docker run -d --name ghost -v /etc/docker/ghost:/ghost-override -e VIRTUAL_HOST=myblog.my-domain.com  dockerfile/ghost

Now you can access your brand new blogging platform at myblog.my-domain.com.

## Sources

You can find the sources in my Github repository [zedtux/nginx-proxy](https://github.com/zedtux/nginx-proxy).

## Docker registry link

As you maybe noticed, this image is hosted on docker.com. You can access the repository at https://registry.hub.docker.com/u/zedtux/nginx-proxy/
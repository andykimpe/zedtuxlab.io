---
layout: post
title: Supprimer tout les emails de mail Unix
date: '2014-10-19 19:57:50'
---

Lorsque vous désirez supprimer tout les emails que votre système a généré, et que donc dans une nouvelle session d'un terminal vous avez ceci:

    You have mail.
    $

Voici comment les supprimers:

    echo 'd *' | mail -N

---
layout: post
title: 'Postfix : Prise en charge des adresses email avec des ''+'''
date: '2014-01-12 20:20:54'
tags:
- linux
- server
- postfix
---

Peut-être connaissez-vous les adresses emails avec un symbole “+” comme par exemple : your.email.address+with+plus+symbol@domain.ltd qui pointera vers your.email.address@domain.ltd.

Ce système de tag pas toujours reconnu (même par les plus grands) peux s’avérer très pratique.

Une fois que j’ai eu vent de cette spécificité, j’ai immédiatement effectué un test sur mon serveur qui utilise PostFix + MySQL pour gérer les adresses virtuelles.

Malheureusement, la requête SQL en charge de vérifier l’existence du destinataire recopie les tags et donc retourne NULL.

Une petite recherche plus tard, et je découvre le sésame :

    sudo postconf -e "recipient_delimiter = +"

Un restart de PostFix et roulez jeunesse ! Ça marche ! :-D 
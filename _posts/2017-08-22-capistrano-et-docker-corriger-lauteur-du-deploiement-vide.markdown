---
layout: post
title: 'Capistrano et Docker: Corriger l''auteur du déploiement vide'
date: '2017-08-22 13:35:00'
tags:
- docker
- capistrano
---

Prenons le cas d'une application Rails, utilisant Docker pour faire tourner les tests sur le serveur de tests continue, avec un déploiement automatique lorsque les tests passent (déploiement continue), avec Capistrano donc.

Lorsque Capistrano fini son travail, il créé un fichier `revisions.log` contenant la branche, le SHA du dernier commit, la version et pour finir le nom de la personne qui a fait le déploiement ... or sur l'instance de Gitlab qui est utilisé, je me suis aperçu que le nom de l'auteur est manquant:

```
00:24 deploy:log_revision
      01 echo "Branch master (at 6d4275f8a7b1637818e981774e9b840c20fa3490) deployed as release 20170822122751 by " >> /srv/myapp_testing/revisions.log

```
_(La phrase se finit par "... 20170822122751 by ", le nom de l'auteur est donc manquant)._

### Corriger le problème

Pour corriger ce soucis, j'ai commencé par regarder comment est générée cette phrase et j'ai trouvé que [la tache Rake `deploy:log_revision`](https://github.com/capistrano/capistrano/blob/v3.9.0/lib/capistrano/tasks/deploy.rake#L188) appelle [une méthode qui utilise une traduction](https://github.com/capistrano/capistrano/blob/v3.9.0/lib/capistrano/dsl.rb#L47) (gérée par I18n) qui prend un paramètre `user: local_user`, et [cette méthode `local_user`](https://github.com/capistrano/capistrano/blob/v3.9.0/lib/capistrano/dsl.rb#L60) fait simplement un appelle `fetch(:local_user)`.

Il suffit donc de définir la variable `:local_user` dans le fichier `config/deploy.rb` de notre application Rails pour régler le soucis.

Idéalement il faudrait prendre l'auteur du dernier commit, ce qui peut se faire avec la commande `git log -1 --pretty=format:'%an'`.

Afin de transmettre l'information de Gitlab à Capistrano, tournant dans notre container Docker, nous allons définir une variable d'environnement dans Gitlab, puis la passer au container, et pour finir, l'utiliser dans le fichier `deploy.rb`.

Dans notre fichier `.gitlab-ci.yml` ajoutez dans un `before_script` la définition de la variable d'environnement, puis passez la dans l'appelle de Docker:

```yaml
...

# Stage "Deploy": Deploys to testing
deploy_staging:
  stage: deploy
  before_script:
    - export LAST_GIT_COMMIT_AUTHOR=`git log -1 --pretty=format:'%an'`
  script:
    - "docker run --rm --volume ${HOME}/.ssh/:/root/.ssh/ --user `id -u` -e LAST_GIT_COMMIT_AUTHOR=\"${LAST_GIT_COMMIT_AUTHOR}\" ${CI_REGISTRY_IMAGE}:latest bundle exec cap testing deploy"
```

Pour finir, mettre à jour le fichier `config/deploy.rb` afin de surcharger la variable `:local_user` avec cette variable d'environnement:

```ruby
...

# Sets the user name to the last commit author
set :local_user, ENV['LAST_GIT_COMMIT_AUTHOR']

```

Vous devriez maintenant avoir l'auteur du dernier commit comme personne ayant déployé.
---
layout: post
title: Remove Docker images no more used
date: '2014-11-29 07:03:14'
tags:
- docker
---

How to go from the following:

<script src="https://gist.github.com/zedtux/83ac3ac18fdcda60888d.js"></script>

to:

<script src="https://gist.github.com/zedtux/6db062d05c0fda97d1e9.js"></script>

easily ?

Just run the following commands (and ignore the warnings and errors ...):

    $ docker ps -a -q --filter "status=exited" | xargs docker rm
    $ docker rmi `docker images --filter "dangling=true" | awk '{ print $3 }' | grep -v IMAGE`

The magic here is the `--filter` flag of docker which allows us to filter the existing images and select only the one which are in status exited (so no running).
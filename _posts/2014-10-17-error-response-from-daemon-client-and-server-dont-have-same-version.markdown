---
layout: post
title: 'Error response from daemon: client and server don''t have same version'
date: '2014-10-17 08:03:37'
tags:
- docker
---

In case you have the following error on OSX:

    docker version
    Client version: 1.3.0
    Client API version: 1.15
    Go version (client): go1.3.3
    Git commit (client): c78088f
    OS/Arch (client): darwin/amd64
    2014/10/17 09:57:49 Error response from daemon: client and server don't have same version (client : 1.15, server: 1.14)

You need to upgrade the boot2docker image:

	$ boot2docker stop
    $ boot2docker download
    $ boot2docker up

Source: http://stackoverflow.com/a/24595426/1996540
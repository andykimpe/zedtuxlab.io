---
layout: post
title: Douane est maintenant open source !
date: '2014-05-02 13:29:14'
tags:
- douane
---

J'ai pris la décision le week-end dernier de libérer le code source de Douane completement.

La première raison est la défaite avec l'Ubuntu Software Center (qui maintenant ne répondent plus du tout ...).
Ensuite la création de paquets me prennait beaucoup de temps, si bien que je ne développé plus.

Par conséquence vous pouvez maintenant revoir le code complètement et le compiler sur n'importe quel distribution Linux.

Jusqu'à aujourd'hui j'ai trouvé 2 articles de blogs (en Russe), et http://douaneapp.com/ a eut un pick de visite à 450 visites avec 400 visites venant de Russie.
Il y a une personne qui est en train de créer les paquets pour Archlinux, et une autre pour Ubuntu.

J'ai aussi mis à jour le site avec des captures d'écrans avec le nouveau GNOME.

Tout le code se trouve sur Github, j'ai écris [ce wiki](https://github.com/Douane/Douane/wiki) qui explique [l'architecture de l'application](https://github.com/Douane/Douane/wiki/Architecture), [les dépendences](https://github.com/Douane/Douane/wiki/Dependencies) et [comment compiler Douane](https://github.com/Douane/Douane/wiki/Compilation).
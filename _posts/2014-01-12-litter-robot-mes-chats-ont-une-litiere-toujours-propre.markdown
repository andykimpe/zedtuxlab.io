---
layout: post
title: 'Litter Robot: Mes chats ont une litière toujours propre'
date: '2014-01-12 19:40:58'
---

![litter_robot_noir](/content/images/2017/08/litter_robot_noir.jpg)

Après avoir fais l’acquisition d’un [iRobot Roomba 560](http://www.irobot.com/fr/home_robots_roomba560.cfm) dont je parle dans cet article, je me suis procuré un [Litter Robot](http://www.robot-litiere.com/).


## robot-litiere.fr: Livraison en 24h !

J’ai commandé mon article sur http://robot-litiere.fr/ qui me l’ont livre dès le lendemain dans l’après midi !

Autant dire que ca commençais bien !

## Installation les doigts dans le nez !

L’installation est tellement simple et évidente que vous n’avez pas vraiment besoin de notice.
Vous posez le socle sur le sol, vous posez le globe sur le socle et roule ! ;-)

Ha oui.. et vous branchez le transfo :-p

## Remplir de litière

Là j’avoue que ca n’a pas été simple !

C’est très mal pensé à ce niveau là ! Il ne faut pas remplir (d’après la notice) par la porte d’entrer, mais par une petite trappe sur le côté.

C’est franchement difficile et bâclé !

## La litière est prête !

Maintenant qu’elle est prête, il ne reste plus qu’a attendre qu’un chat se dévoue pour y aller faire ses affaires…

## Comment aider le(s) chat(s) a s’y intéresser ?

Voilà la question que vous devez vous poser depuis que vous avez vue cette litière.

Je vous rassure, tout les chats s’habitueront ! La différence se fera sur la rapidité d’adaptation.

### Les jeunes chatons

Alors là, c’est avec les plus jeunes que ça va le plus vite !

Ils se jettent dedans, sans peur, ni reproche, et roule !

### Les chats adultes

C’est ici que commence les ennuies…

Mon premier chat a maintenant 3 ans, et est assez peureux. Il lui a fallut 2 semaines pour y aller de lui même.

Croyez moi, si mon Hiro est allé dans cette litière, tout les chats du monde iront dedans !! :)

### Et si vraiment le chat n’y va pas ?

Bon alors si ce n’est vraiment pas faisable, l’appareil est vendu avec 90 jours d’essais. Dans ce cas, vous n’aurez qu’a bien nettoyer la litière et la renvoyer dans son carton.

## Astuces pour aider le chat à y aller tout seul

Il en existe plusieurs.

Première chose à faire, laisser l’ancienne litière a côté de la nouvelle.

Ensuite, vous pouvez verser une tasse de l’ancienne litière dans la nouvelle.

Ne jamais forcer le chat a y aller. Il ira a un moment ou l’autre. Le forcer a y aller emmènera le chat a en avoir plus peur.

Tout au mieux, ce que j’ai fais avec mon chat peureux, c’est le prendre en le caressant, puis je me suis mis a genoux devant la litière, puis j’ai posé Hiro sur mes jambes tout en le tenant de moins en moins, jusqu’à ne plus le tenir du tout. Le chat s’intéresse a la litière et la renifle. Hiro regarde tout le temps a l’intérieur du globe avant de rentrer dedans, pour être sûre qu’il n’y a pas de monstres, aliens ou autres qui se cachent :)
Étant sur mes jambes, il se sent plus en confiance et passe même plus facilement la tête dans le globe.


## Est-ce mieux pour les odeurs ?

Hiro est du genre a attendre que je soit à table pour aller faire popo .. C’est toujours un plaisir ….

Maintenant que la nouvelle litière est là, plus d’odeurs ! En ce qui concerne la récupération des popo, un bac est prévu à cette effet, il faut simplement y mettre un sac poubelle (2 ou 3 fournie pour le début) et le changer dès qu’il est plein et en fonction du nombre de chat.

## Conclusion

En bref, je suis très content de la nouvelle litière, un brin futuriste, pratique, et pas trop bruyante, si vous aimer le high tech, etc.. et que vous avez de l’argent …. elle est pour vous !

Son prix est très élevé, mais bon, une fois qu’elle est en place, plus trop besoin de tourner au tour. Surtout avec des chats exigeant, qui râlent quand il ressortent d’une litière sale :)

Surtout, n’ayez pas peur que votre chat ne rentre pas dedans, surtout si il est jeune !
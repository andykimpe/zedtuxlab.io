---
layout: post
title: 'Lynis: Suggestion: Install package apt-show-versions for patch management
  purposes [test:PKGS-7394]'
date: '2014-01-17 18:08:26'
tags:
- server
- security
---

If Lynis list the following suggestion:

> Suggestion: Install package apt-show-versions for patch management purposes [test:PKGS-7394]

Then install the package like this:

    sudo apt-get install apt-show-versions
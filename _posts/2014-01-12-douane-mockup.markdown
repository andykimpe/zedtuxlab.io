---
layout: post
title: 'Douane: Mockup'
date: '2014-01-12 20:39:00'
tags:
- my-developments
- douane
---

Since [the announcement of the project](/announcing-douane/) until [my last post](/douane-daemon-is-in-the-pipe/) about Douane, I didn’t post any visual things.

That’s the goal of this article :)

 

One feature of Douane will be to show, in real time, the network activities.
To show this activities, I’ve been inspired by [Little Snitch](http://www.obdev.at/products/littlesnitch/index.html), and I did 2 gauges to show in and out going traffic.
(I miss Little Snitch on Ubuntu, so that’s why I’m so inspired by it. But I will not do a clone only. I want more features.)
As I’m a [Ubuntu](http://www.ubuntu.com/) fan, I made it in order to be well integrated to [Unity](http://unity.ubuntu.com/), but also to [GNOME-Shell](http://live.gnome.org/GnomeShell).

## Unity Applet

![douane_unity](/content/images/2017/08/douane_unity.png)

## GNOME-Shell extension

![douane_gnome_shell](/content/images/2017/08/douane_gnome_shell.png)
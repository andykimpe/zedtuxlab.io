---
layout: post
title: Designe Pattern MVC
date: '2014-01-12 01:53:22'
tags:
- design-pattern
---

Je ne connaissais pas encore le Design Pattern MVC (Modèle-Vue-Contrôleur) donc je vais vous le présenter.

Voici une représentation vraiment clair que j'ai trouvé sur internet:

![MVC](http://image.zedroot.org/pics/e2a3930a0e50d2cabbeac9f87dd4fcc3.gif)

Ce schéma, représente ce Design Pattern.
Globalement, l’idée est bien représenter: TOUT passe par un contrôleur .

Encore un petit schéma pour la route, piqué sur le net, comme celui d’avant:

![MVC2](http://image.zedroot.org/pics/8ae3be293fe61487e5793ae1b0ca0059.png)

Grossièrement, le principe est de définir une action, que le contrôleur va comprendre, et va appeler un phtml (dans le cas de Zend) nommé comme cette dite action.

Donc, pour exemple, si j’ai l’URL http://localhost/index/ajouter le contrôleur `IndexControleur.php` sera appelé, contenant toutes les actions que l’index contient (on peut dire, je pense, qu’une action représente un lien hypertexte), puis c’est `ajouterAction.phtml` qui sera appelé pour afficher l’action.

Si vous connaissez la POO (Programmation Orienté Objet), l’architecture des dossiers du design pattern MVC coulera de source.
Elle consiste surtout à un dossier application, contenant les contrôleurs, les vues, les modèles, etc …, puis un dossier public pour l’index et le `.htaccess` pour l’URL rewrite (mod_rewrite d’apache).

Maintenant que j’ai fais quelques pas avec le FrameWork Zend, et le Design pattern MVC, je trouve ce framework vraiment intéressant !
La construction de formulaires, comprenant sa validation; les Db Modèles; le système de contrôleurs; …

Il est certain que si vous maîtrisez ce framework, vous pourrez construire des sites web très rapidement, voire même des RIA ( Riche Internet Applications ) en un rien de temps !

Les gens qui pense que

> Ce que j’écris est meilleur, plus fiable,…

comme le faisaient nos père, devient aujourd’hui une vaste connerie !

Les FrameWorks sont vraiment fais pour avancer, pour ne pas perdre de temps à ré-inventer la roue, surtout si elle à été faite presque à la perfection.
Il ne faut pas avoir peur de la “rigidité” d’un FrameWork… Bien souvent, ils sont largement flexible pour ne pas vous dérangez dans votre travail (une fois que l’on à appris à ce servir de ce dernier ! )
Sauf si vous ne connaissez que la programmation procédurale ….. :s
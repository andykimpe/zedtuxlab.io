---
layout: post
title: Surveiller son Load Average, et être alerté par mail
date: '2014-01-12 01:58:50'
tags:
- linux
- maintenance
---

Ce matin, un serveur que je gère à ma boite, s’est retrouvé être très lent ( Load Average entre 3 et 5 sur un CPU dual core :-/ ).

J’ai donc mis un en place un script qui vérifie le load average, et si il dépasse la limite fixé, envoie un email.
Ce script je l’ai trouvé dans un article posté par Dimitar Tanev sur le blog tkdev.org.

Avant d’aller plus loin, il est important de savoir comment lire le load average et donc, avant ca, savoir ce qu’est le Load average.

## Savoir ce qu’est le Load average
Le Load average est la représentation de la charge de la machine sur 5min, 10min et 15min.
Le calcul prend en compte le nombre de processus qui consomme du CPU, ou qui attendent pour avoir du temps d’exécution sur le CPU.

## Comment lire le load average
Maintenant, tout dépend de l’architecture de votre machine.
Si vous possédez 1 CPU avec 1 seule cœur, le Load average ne doit pas dépasser 1.
Sinon, si vous possédez 1 CPU avec 4 cœurs, votre Load average ne doit pas dépasser 4.
Il faut donc faire l’addition des cœurs pour avoir la limite.

## Le script
Le script va, donc, lire le fichier /proc/loadavg pour avoir le load average, la valeur est écrite dans le fichier de log /var/log/load.log, puis comparé à la limite fixé dans la variable max_loadavg.
Si la limite est dépassé, un e-mail part.

Voici le script :

```bash
# This script is free for ALL
# Visit us at http://www.tkdev.org/articles/ for updates or more information.
#
# Author: Dimitar Tanev, 25th February 2009
# E-Mail: dimitar@linuxdevgroup.org
#
 
#!/bin/bash
 
# Load average ALERT value.
max_loadavg=3.5
 
loadavg=$(cat /proc/loadavg | awk '{print $1}')
 
# Comment the line below if you do not want to log the load average in a file.
echo "Load Average:$loadavg on `date`" &gt;&gt; /var/log/load.log
 
# Email subject
SUBJECT="`hostname` LOAD AVERAGE ALERT $loadavg(&gt;$max_loadavg)"
 
# Email To?
EMAIL="mailbox@domain.com"
 
# Email text/message
EMAILMESSAGE="Load Average:$loadavg on `date`"
 
if [ $(echo "$loadavg &gt;= $max_loadavg"|bc) -eq 1 ]
 
then
 
# Send an Email using /bin/mail
echo $EMAILMESSAGE | mail -s "$SUBJECT" "$EMAIL"
 
fi
 
exit
```

## Mise en place

Coté installation, vous rendez le script exécutable, puis, il suffit de créer une régle cron.
Par exemple, pour l’exécuter toutes les 3 minutes :

    */3 * * * * nice -n 15 /root/loadavr.sh &> /dev/null

Voila, c’est bon, vous devriez être avertis en cas de pépeins ! :)
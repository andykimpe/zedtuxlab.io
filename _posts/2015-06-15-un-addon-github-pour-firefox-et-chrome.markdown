---
layout: post
title: Un addon Github pour Firefox et Chrome
date: '2015-06-15 21:04:28'
---

[Octotree](https://github.com/buunguyen/octotree) est un addon pour Firefox et Chrome qui vous permettra de naviguer dans un dépôt Github avec une arborescence en arbre comme vous pouvez le voir sur la capture d'écran au dessus.

Veuillez vous référer au README du dépôt pour l'installation.
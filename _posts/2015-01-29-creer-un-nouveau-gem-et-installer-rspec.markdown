---
layout: post
title: Créer un nouveau gem et installer RSpec
date: '2015-01-29 12:46:15'
---

Si comme moi vous oubliez toujours comment installer RSpec dans un gem fraichement créé, voici un petit mémo:

    $ cd dans/le/dossier/de/developpements/
    $ bundle gem nom-du-gem
    $ cd gem nom-du-gem

Modifiez le fichier .gemspec pour y ajouter:

    spec.add_development_dependency 'rspec'

Puis continuez dans le terminal:

    $ bundle
    $ rspec --init

Un fichier `.rspec` et `spec/spec_helper.rb` seront créé.

Maintenant modifiez le fichier `Rakefile` afin qu'il resemble à ceci:

    require 'bundler/gem_tasks'
    require 'rspec/core/rake_task'
    
    RSpec::Core::RakeTask.new(:spec)
    
    task default: :spec

À partir de ce moment, lancer la commande `rake` dans un terminal exécutera les tests avec RSpec.
---
layout: post
title: Git changed files
date: '2015-02-12 13:04:02'
tags:
- git
---

You need to see the list of modified files in your branch compared to master ?

This is for you !

In your `~/.gitconfig` file add the following in the `[alias]` section:

    changedfiles = "!git diff --name-only master `git rev-parse --abbrev-ref HEAD`"

Now executing `git changedfiles` will show your them :)
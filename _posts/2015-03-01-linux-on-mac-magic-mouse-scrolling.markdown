---
layout: post
title: 'Linux On Mac: Magic mouse scrolling'
date: '2015-03-01 16:42:31'
tags:
- linux-on-mac
---

The magic mouse is working out-of-the-box but is very sensible !

First thing to do is to change the speed of the pointer in the User & System settings and then in the Mouse & Touchpad panel:

![user-sytem-settings_mouse-touchpad](/content/images/2017/08/user-sytem-settings_mouse-touchpad.png)

Now regarding the wheel to scroll in window content, there is no nice GUI way. Open a terminal and execute the following commands:

    $ echo 45 | sudo tee /sys/module/hid_magicmouse/parameters/scroll_speed
    $ echo N | sudo tee /sys/module/hid_magicmouse/parameters/scroll_acceleration

Now you mouse should be better usable.

#### Update 2015

The value `45` is a little bit too slow to me (I'm working during the day with OS X and Linux in the evening) so I've updated the `scroll_speed` to `49` which looks closer to the OS X behavior.
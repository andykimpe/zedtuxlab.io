---
layout: post
title: 'My first Gem: Inverse of number_to_human_size'
date: '2014-01-12 20:30:53'
tags:
- ruby
- contribution
- my-developments
---

After 2 years of [Ruby On Rails](http://rubyonrails.org/) development at my work, I finally created my first gem [after having contributed](https://github.com/radar/dotiw/pull/8) to the [DOTIW gem](https://github.com/radar/dotiw).

We was looking to a gem that do the inverse of the [ActionView::Helpers::NumberHelper#number_to_human_size](http://apidock.com/rails/ActionView/Helpers/NumberHelper/number_to_human_size).

As we didn’t find anything, I’ve decided to write it a last week-end and here is [the human_size_to_number gem](https://github.com/zedtux/human_size_to_number) ! :) 
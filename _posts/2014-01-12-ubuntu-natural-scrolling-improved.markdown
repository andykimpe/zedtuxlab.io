---
layout: post
title: Ubuntu natural scrolling improved
date: '2014-01-12 20:41:10'
tags:
- python
- contribution
- my-developments
---

Maybe, some of you are using the [GNOME Applet Natural Scrolling](https://github.com/cemmanouilidis/naturalscrolling)?

If not, and you want to know what is Natural Scrolling, then look at [the README file](https://github.com/cemmanouilidis/naturalscrolling/blob/master/README.md).

You maybe know too that I’m the collaborator [cemmanouilidis](https://github.com/cemmanouilidis).
Then I’m implemented the next major feature: **Multiple devices support**.
Where do we stand?

Short answer is close to the end.

I already have release a first testing version. To do that, I’ve created a **naturalscrolling-testing** package available in [my Apple naturalscrolling PPA](https://launchpad.net/~zedtux/+archive/naturalscrolling).
There already exists [an issue](https://github.com/cemmanouilidis/naturalscrolling/issues/19) on [github.com](https://github.com/) about missing dependencies. So sorry about that.

If you’ve tested it, and have a bug, please fill in [a new issue](https://github.com/cemmanouilidis/naturalscrolling/issues/new) following instructions from [the Natural Scrolling Wiki page](https://github.com/cemmanouilidis/naturalscrolling/wiki).
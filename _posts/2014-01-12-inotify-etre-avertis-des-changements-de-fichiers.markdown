---
layout: post
title: 'inotify: Être avertis des changements de fichiers'
date: '2014-01-12 17:01:12'
tags:
- linux
- command-line
---

Je vient de résoudre une demande, grâce à la ligne de commande, en 10min !! :)

Le but étant de contacter un serveur, lorsqu’un nouveau fichier est déposé dans un répertoire, afin que cet autre serveur viennent les traiter.

`inotify` est un module, intégré dans le noyau de Linux, depuis sa version 2.6.23.
Sont but est de créer des événements sur le système de fichiers.

`syslog` est toute une application, en client/serveur, qui permet à n’importe quel application, de créer des logs, et ceux, même au travers d’un réseau !

Voilà ! Ce sont tout les outils dont j’ai besoin !!

Je vais donc, avec `inotify`, surveiller mon dossier, et créer une entrer dans mes logs ([redirigé vers mon autre serveur](/syslog-log-en-réseau/)) avec `logger`, contenant le nom du fichier créé.

Il me reste plus qu’a exécuter ma ligne de commande que voici :

    inotifywait -m -e create /var/upload/ –format ‘%f’ | logger -p ftp.alert -t ftpMaintenance

L’argument `-m` permet de monitorer, c’est à dire, que une fois l’événement exécuter, `inotifywait` ne s’arrête pas, et se remet à l’écoute du prochain événement.

L’argument `-e` permet de créer l’événement. Ici je demande de m’appeler quand un fichier est créé. Il y en a plein… voire man notifywait.

Ensuite, ont trouve le chemin à surveiller.

Le `–format` permet de définir comment doit être la sortie. Ici le `%f` permet d’avoir le nom du fichier uniquement.

Voili, voilou !! ^^

Le résultat:

    Jul 4 12:00:19 zUbuntu ftpMaintenance: apache-maven-2.2.0-bin.tar.gz

C’est pas magnifique tout ca !!? ;)

Moralité … Linux c’est trop puissant !! :D 
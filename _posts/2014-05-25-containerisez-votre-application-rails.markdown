---
layout: post
title: Containérisez votre application Rails
date: '2014-05-25 16:28:22'
tags:
- docker
---

## Containeriser?

L'idée est de créer un container Linux dans lequel vous y placer votre application et toutes ses dépendences afin qu'elle puisse fonctionner.

Une fois ce container crée, il suffira de le mettre n'importe où afin de faire fonctionner votre application.

Premier avantage: vous n'aurez plus d'opération de maintenance sur votre ou vos serveurs.
Autre avantages: les containers sont complement vérouillé, aucun port n'est ouvert sauf si vous le demandez explicitement.

## Rails

Ici je vais démontrer comment containeriser une application Ruby on Rails, mais c'est bien entendu adapté a plein d'autres languages et frameworks.

Le plus important dans mon exemple est l'architecture de l'application et donc celle des containers afin de faire fonctionner notre application et que nous puissions profiter de la fonctionnalitée la plus importante des containers: la montée en charge.


## L'application d'exemple

Peux-être connaissez vous l'application que j'ai développé il n'y a pas trop longtemps: [brewformulas.org](http://www.brewformulas.org/).
C'est une application Rails qui utilise une base de donnéees pour stoquer les formules installable avec [Homebrew](http://brew.sh/) (sur Mac) et des tâches qui tournent en arriere pour importer toutes les nuits les formules.

[Le code source](https://github.com/zedtux/brewformulas.org) se trouve sur [Github](https://github.com/).

### La pile de technologies

Voici donc tout ce dont nous avons besoin pour faire tourner cette application:

Il nous faudra au moins 1 instance de :

 - [PostgreSQL](http://www.postgresql.org/) (Il est tout a fait envisageable d'utiliser n'importe quel autres base de données supporté par Rails)
 - [Redis](http://redis.io/), qui est utilisé pour enregistrer les tâches que Sidkiq fera tourner
 - [Sidekiq](http://sidekiq.org/) qui est la bibliothéque utilisé pour exécuter des tâches en font
 - [Rails](http://rubyonrails.org/) qui fera donc tourner l'application web et qui sera l'application qui répondra aux requêtes des visiteurs

[Newrelic](http://newrelic.com/) sera installé sur les différents serveurs afin d'avoir un oeil sur le bon fonctionnement de l'application et aussi afin de mettre en évidence la raison de ralentissements.

### L'infrastructure

Étant une toute petite application, utiliser plusieurs serveurs pour faire tourner brewformulas.org peux paraître exagéré, mais le but est que vous puissiez imaginer votre application (qui sera surement de plus grande importance) dans le même contexte et donc profiter des avantages de la containerisation.

Nous allons donc utiliser en tout 5 serveurs:

 - Un répartiteur de charge
 - Un serveur PostgreSQL
 - Un serveur Redis
 - Un serveur pour sidekiq
 - Un serveur pour l'application Rails

Le but sera donc de pouvoir augmenter et réduire le nombre de serveurs qui feront tourner l'application afin de pouvoir prendre en charge un grand nombre de visites a un moment d'affluence, puis de réduire le nombre de machines afin de ne pas payer pour rien des serveurs inutilisés.

Nous allons donc utiliser quatre containers: un pour PostgreSQL, un autre pour Redis, puis un pour sidekiq et finalement un pour l'application Rails.

## Containeriser

Voici le coeur du sujet: La création du container qui va contenir nos dépendences et notre application Ruby on Rails.

Cette étape est rendu facile grâce a [Docker](https://www.docker.io/).
Docker fournis une application en ligne de commnade qui vous permettra de créer et gérer vos containers. Vous pouvez commencer par faire [leur tutoriel](https://www.docker.io/gettingstarted/) (en Anglais) afin de vous familiariser avec Docker.

[Lire mon article "Containeriser avec Docker".](http://blog.zedroot.org/containeriser-avec-docker)

## Expedier votre container

Maintenant que votre container est prêt, il doit être expédié afin de pouvoir le déployer sur un serveur.

Pour ce faire nous allons utiliser la plate forme [Quay.io](https://quay.io/).

[Lire mon article "Stoquez vos containers sur Quay.io".](http://blog.zedroot.org/stoquez-vos-containers-sur-quay-io)

## Déployer les containers

Pour finir il faut déployer nos containers et les reliers ensemble afin que notre application soit fonctionnelle.

Un peu avant, je vous disais que nous voulons pouvoir augmenter et réduire le nombre d'instances et donc de serveurs a volonter, afin de pouvoir gérer une grande affluence sur votre site, mais aussi pour éviter un coût trop important.
L'idée est donc de ne plus payer un serveur au mois, mais à l'heure, et ca, seule les services en nuages peuvent le fournir.

Vous pourrez donc utiliser des services tel que [Amazon AWS](http://aws.amazon.com/fr/) (qui viennent récement d'implémenter Docker dans leur infrastructure), mais je vais plustot vous présenter une plate forme tres prometteuse et simple d'utilisation que j'ai découvert récement.

[Lire mon article "Tutuuuum".](http://blog.zedroot.org/tutuuuum)

# Conclusion

Comme vous l'aurez surement compris, Docker est un outil vraiment révolutionnaire dans le sense où il nous permet de créer ces fameux containers Linux qui sont à mes yeux l'avenir du déploiement d'application.

Les possibilitées d'utilisation de Docker sont infinies et pourrait même devenir dans le future le nouveau **apt-get** ou **yum** comme le fais déjà [CoreOs](https://coreos.com/).
Mais ceci est une autre histoire :-)
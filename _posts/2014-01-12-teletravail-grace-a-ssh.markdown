---
layout: post
title: Télétravail grace à SSH
date: '2014-01-12 20:27:38'
tags:
- ssh
---

Voici comment travailler depuis la maison, grâce à notre amis SSH.

Voici la configuration:

![Untitled1](/content/images/2017/08/Untitled1.png)

Alors votre entreprise dispose de plusieurs serveurs, dont un, le “A”: Celui sur lequel vous avez accès. Allez, vue que c’est très souvent le cas, disons qu’il y a un dépôt [Git](http://git-scm.com/) dessus :)
Votre entreprise vous à créé un compte SSH sur son Gateway.

Disons que vous désirez cloner un repository hébergé sur le serveur A (Ce dépôt doit contenir la clé SSH de votre PC. voir gitosis).
Il faut donc atteindre le serveur A au travers du Gateway. Cela ce fait très facilement avec SSH.

Pour ouvrir un tunnel entre vous et le serveur A:

    ssh -L 4444:serveur_a:22 nom_utilisateur@gateway -N

Cette commande va ouvrir le tunnel sur le port local 4444 de votre machine (ce qui permettra d’envoyer vos requêtes dans le tunnel avec localhost:4444) vers le port 22 du serveur A.
Puis à la fin précisez votre nom d’utilisateur que votre entreprise vous a fournis puis le nom ou l’adresse ip du Gateway.
Le -N permet de ne pas avoir de prompt. Si vous désirez pouvoir exécuter des commandes sur Gateway sans ré-ouvrir une connexion SSH, enlevez le.

Voilà! Maintenant un tunnel à été construit entre vous et le serveur A.

Pour cloner un dépôt Git lorsque vous êtes au travail vous faites:

    git clone git@serveur_a:depot.git

Il faut juste le changer pour utiliser votre tunnel SSH comme suit:

    git clone ssh://git@localhost:4444/depot.git
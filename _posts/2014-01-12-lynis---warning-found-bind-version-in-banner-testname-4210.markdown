---
layout: post
title: 'Lynis – Warning: Found BIND version in banner [test:NAME-4210]'
date: '2014-01-12 20:46:20'
tags:
- linux
- server
- security
---

If Lynis list the following warning:

> Lynis – Warning: Found BIND version in banner [test:NAME-4210]

You will fix it by editing the file `/etc/bind/named.conf.options` and add the following line into the options node:
	
	version "Not disclosed";

And restarting the service:

	sudo /etc/init.d/bind9 restart

 

At anytime, if you’re encountering any issue, you can use the following command line to check what is wrong:

	/usr/sbin/named -g
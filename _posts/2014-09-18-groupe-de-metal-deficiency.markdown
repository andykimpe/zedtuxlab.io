---
layout: post
title: 'Groupe de Métal : Deficiency'
date: '2014-09-18 12:11:51'
---

![deficiency_logo](/content/images/2017/08/deficiency_logo.png)

Je suis allé le week-end dernier à un évènement ici au Luxembourg qui incluant le concert d'un group de métal. J'y suis aller sans grande attentes mais j'ai vraiment été agréablement surpris par le groupe !

Ils ont réussi à mettre une super ambiance et à faire bouger autant des gens qui n'écoutent pas de métal habituellement, autant des gens qui en écoute tout les jours, comme moi.

Deficiency est un groupe Français composé de 4 membres: Un guitariste principale, un guitariste rythmique, un bassiste et un batteur. Ils sont vraiment simpas et cool et s'était vraiment avec plaisir que j'ai acheté leur albums et T-shirt.

![deficiency_band](/content/images/2017/08/deficiency_band.jpg)

Bon restons simple, essayez, et vous verrez :-)

Premièrement je vous suggère de regarder ma vidéo du concert montrant un des meilleur moment du concert qui se trouve sur Youtube: https://www.youtube.com/watch?v=eQtkqaL8EzE.

Par la suite vous pouvez écoutez leur albums sur Spotify en cliquant sur [ce lien](http://open.spotify.com/artist/2U3uwxpSBsfzUwwCpeZ2T9).

Et pour finir vous pourrez acheter leur CD et t-shirts sur [le site officiel de Deficiency](http://www.deficiency.fr/)!

### Mise à jour

Voici une vidéo où les guitaristes préparent de nouvelles compositions : https://www.youtube.com/watch?v=Ps240qcmX04
---
layout: post
title: Tester rapidement la connexion à une WebSocket
date: '2019-06-17 11:45:34'
---

Une petite note pour moi, lorsqu'il faut vérifier une connexion à un serveur de WebSocket, aller sur le site, puis dans la console du navigateur y entrer :

<script src="https://gist.github.com/zedtux/0932fd1dac3c77ad074b02754b05ecef.js"></script>

La connexion s'établie tout de suite, et les premiers messages apparaissent :

```
WS Open
VM1443:5 WS message: MessageEvent {isTrusted: true, data: "{"type":"welcome"}", origin: "wss://ws.domain.eu", lastEventId: "", source: null, …}
VM1443:5 WS message: MessageEvent {isTrusted: true, data: "{"type":"ping","message":1560771854}", origin: "wss://ws.domain.eu", lastEventId: "", source: null, …}
VM1443:5 WS message: MessageEvent {isTrusted: true, data: "{"type":"ping","message":1560771857}", origin: "wss://ws.domain.eu", lastEventId: "", source: null, …}
```

Puis pour arrêter la WebSocket :

```
ws.close()
```

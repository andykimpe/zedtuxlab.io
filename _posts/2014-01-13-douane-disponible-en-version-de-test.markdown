---
layout: post
title: Douane disponible en version de test!
date: '2014-01-13 12:56:59'
tags:
- mes-developpements
- douane
---

Finalement c’est le week-end dernier que j’ai enfin publié une version de test de mon firewall Douane.

Cette version de test comporte des bugs (reporté sur la page https://github.com/zedtux/Douane/) mais je vous invite à suivre le compte Twitter de Douane (à l’adresse https://twitter.com/douaneapp) afin d’être mis au courant des évolutions.

Je travail principalement à résoudre les problèmes avec le paquet debian (qui va bientôt devenir les paquets pour les différentes version d’Ubuntu).

Vous pouvez donc télécharger la version de test à la page https://github.com/zedtux/Douane.

N’hésitez pas à remonter les bugs et surtout les idées pour améliorer l’application. Je suis complètement ouvert aux changements.
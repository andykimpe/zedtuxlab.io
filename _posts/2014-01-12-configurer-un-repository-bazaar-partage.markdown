---
layout: post
title: Configurer un repository Bazaar partagé
date: '2014-01-12 18:31:54'
tags:
- vcs
- bazaar
---

![Logo Bazar](/content/images/2017/08/bazaar_logo1-1.png)

**Bazaar** est un gestionnaire de version écrit en [python](http://www.python.org/), par [Canonical](http://www.canonical.com/).
A la base, il s’agit d’un fork de [GNU Arch](http://www.gnu.org/software/gnu-arch/). [[Historique de Bazaar](http://bazaar-vcs.org/HistoryOfBazaar)]

Le but de ce programme est de créer des versions de fichiers afin de pouvoir revenir en arrière à tout moments, créer des branches ( donc un répertoire pour une version final ).
Il fait partis de la même catégorie de programme que

 - Subversion http://subversion.tigris.org/
 - Git http://git-scm.com/
 - Mercurial http://mercurial.selenic.com/wiki/
 - etc…

# Pourquoi Bazaar ?

Jusque là, j’eu utilisé Subversion. Maintenant ce n’est pas que je veuille changer, ou que Subversion ne soit pas bien, mais découvrir d’autres outils est très utile, car lorsque l’on travaille avec de nouvelles personnes, et que l’on doit faire un travail d’équipe, si les outils sont déjà connus, c’est un problème de moins !

Puis, en tant que passionné de programmation, c’est tout à fait naturel de voir les différentes possibilités mises à ma disposition, qui pourraient améliorer mes conditions lorsque je développe.

Maintenant, Bazaar a comme intérêt la simplicité !
C’est à dire, avoir à entrer le moins de commandes possible, configurer le moins de choses possibles, le tout, en restant collaboratif avec n’importe qui !
N’est ce pas là les critères que recherche un bon informaticien ? :-D

**N’importe quel espace disque est un repository potentiel !**

En effet, voici pour moi, le plus gros avantage de Bazaar !
N’importe quel espace disque est un repository potentiel. Une clé USB, un serveur, une carte flash, un NAS, un iphone, etc… peuvent heberger un repository Bazaar.
Pas besoin d’installer de version serveur!

Et ceux, grâce au fait que le repository est décentralise, et qu’il n’y a pas besoin d’installer de serveur.
Tout se passe dans le dossier .bzr/ créé lorsque vous lancez le `bzr init`.

**Bazaar est portable**

Comme il a été écrit en Python, il fonctionne sous tout les OS pouvant lancer python.

**Bazaar peut mimer les autres**

Bazaar supporte plusieurs manières de gérer vos sources. Il peut travailler comme Git, en décentralisé, ou encore comme Subversion, qui est centralisé, etc…

#Créer un repository sur un serveur

Comme dit plus tôt, pas besoin d’installer quoi que ce soit. Il faut juste paramétrer l’accès par sftp, par exemple, qui est le plus simple.

Il faut aussi voir à **chrooter** (Changer le répertoire root `/` pour bloquer l’utilisateur dans son dossier personnel.) les utilisateurs, pourqu’ils ne puissent uniquement envoyer des données dans le dossier bien défini pour héberger le repository.

Pour chrooter les utilisateurs, rassurez vous, il n’y a pas de fichiers à copier, etc … juste une petit paramétrage d’SSH, c’est tout.
(Re-créer un environnement (`/dev`, `/etc`, `/var` …) pour chrooter un utilisateur est utile si il a un accès à un shell, un prompt. Ici il n’aura que `sftp`, donc ne pourra pas exécuter de commandes.)

### 1. Créer un groupe

Pour simplifier le paramétrage du serveur, nous allons créer un groupe, qui sera configuré pour les projets bazaar. Tout nouveaux développeurs qui y seront inclut, seront immédiatement prêt pour commiter dans le repository bazaar.

    groupadd bazaar

### 2. Créer le répertoire du repository

Bon .. c’est un bête mkdir ^^

    mkdir /var/bazaar/

Donc, où bon vous semblera !

### 3. Créer les utilisateurs pour les développeurs

Maintenant que notre groupe existe, ainsi que son répertoire personnel, ajoutons les comptes !

    useradd –home-dir /var/bazaar/ –gid bazaar –shell /usr/lib/sftp-server zedtux
    useradd –home-dir /var/bazaar/ –gid bazaar –shell /usr/lib/sftp-server dev01
    useradd –home-dir /var/bazaar/ –gid bazaar –shell /usr/lib/sftp-server dev02

### 4. Fixer les permissions par defaut

Si là, vous commiteriez quelque chose, ca fonctionnerait, mais vous seul seriez capable de modifier le projet. Ceci est dut à un problème de permission.
Nous avons créer un groupe bazaar afin de pouvoir jouer sur les permissions pour autoriser tout les gens de ce groupe à modifier les sources.
Mais par défaut, les permissions sont : utilisateur peut tout faire, le groupe juste lire et executer, et pareil pour les autres comptes et groupe.

Nous allons donc envelopper sftp dans un script bash pour appliquer des permissions.

D’abord, en renomme l’exécutable de sftp, pour que notre script prenne sa place :

    mv /usr/lib/sftp-server /usr/lib/sftp-server.orig

Maintenant écrivons le script :

    nano /usr/lib/sftp-server

    #!/bin/sh
    umask 002
    /usr/lib/sftp-server.orig “$@”

Une fois sauvegardé, nous le rendons executable :

    chmod a+x /usr/lib/sftp-server

Voila ! Tout fichier envoyé aura les permissions du groupe en écriture, donc tous les utilisateurs du groupe pourront modifier les fichiers des autres.

# Conclusion

Installer Configurer un serveur pour héberger un repository bazaar prend 2 minutes ( quand on connais biensur, je l’admets, mais un serveur est généralement géré pas un admin qui est sensé connaitre le système de permissions et de gestion d’utilisateur ;-) ), ne demande aucune installation, aucune modification de fichier de configuration et de redémarrage.
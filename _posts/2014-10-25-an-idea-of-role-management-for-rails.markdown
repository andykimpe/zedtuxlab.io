---
layout: post
title: An idea of role management for Rails
date: '2014-10-25 15:13:55'
---

Just a reminder for me.

I was looking to the existing gems for authorisations.

[Pundit](https://github.com/elabs/pundit) is nice in the way that it is segragating the policies within an `app/policies/` folder but it is too complex or complete.

[Canable](https://github.com/pablobm/canable) is nice in the way it is very simple.

I'd like to create a gem with will merge both things:

- Keep it simple
- Segregate policies within an `app/policies/` folder
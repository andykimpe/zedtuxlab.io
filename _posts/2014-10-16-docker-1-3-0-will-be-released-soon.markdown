---
layout: post
title: Docker 1.3.0 will be released soon !
date: '2014-10-16 13:27:29'
tags:
- docker
---

In the case you are a Docker fan like me, you will be excited read this tweet:

<blockquote class="twitter-tweet" data-cards="hidden" lang="fr"><p>.<a href="https://twitter.com/jpetazzo">@jpetazzo</a> says that v1.3 might be released in few hours. <a href="https://twitter.com/hashtag/dockermeetup?src=hash">#dockermeetup</a> hosted by <a href="https://twitter.com/VMware">@VMware</a> Beijing <a href="http://t.co/SJT6ZKN342">pic.twitter.com/SJT6ZKN342</a></p>&mdash; Docker (@docker) <a href="https://twitter.com/docker/status/522715386010415104">16 Octobre 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

There are 914 commits since the version 1.2.0 at the moment I'm writing this article.

## 1.3.0 release note

This version fixes a number of bugs and issues and adds new functions and other improvements. These include:

#### New command: docker exec

The new docker exec command lets you run a process in an existing, active container. The command has APIs for both the daemon and the client. With docker exec, you'll be able to do things like add or remove devices from running containers, debug running containers, and run commands that are not part of the container's static specification.

#### New command: docker create

Traditionally, the docker run command has been used to both create a container and spawn a process to run it. The new docker create command breaks this apart, letting you set up a container without actually starting it. This provides more control over management of the container lifecycle, giving you the ability to configure things like volumes or port mappings before the container is started. For example, in a rapid-response scaling situation, you could use create to prepare and stage ten containers in anticipation of heavy loads.

#### Tech preview of new provenance features

This release offers a sneak peek at new image signing capabilities that are currently under development. Soon, these capabilities will allow any image author to sign their images to certify they have not been tampered with. For this release, Official images are now signed by Docker, Inc. Not only does this demonstrate the new functionality, we hope it will improve your confidence in the security of Official images. Look for the blue ribbons denoting signed images on the Docker Hub. The Docker Engine has been updated to automatically verify that a given Official Repo has a current, valid signature. When pulling a signed image, you'll see a message stating the image you are pulling has been verified. If no valid signature is detected, Docker Engine will fall back to pulling a regular, unsigned image.

#### Other improvements & changes

We've added a new security options flag that lets you set SELinux and AppArmor labels and profiles. This means you'll longer have to use `docker run --privileged` on kernels that support SE Linux or AppArmor.
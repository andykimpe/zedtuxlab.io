---
layout: post
title: L’iPhone fonctionne mieux sous Ubuntu Lucid Lynx 10.04 !
date: '2014-01-12 19:30:40'
tags:
- linux
---

![ubuntu_10_04_iphone](/content/images/2017/08/ubuntu_10_04_iphone.png)

Mise à jours: Aller lire mon nouvel article: [L’iPhone fonctionne ENCORE mieux sous Ubuntu Lucid Lynx 10.04 !](/l’iphone-fonctionne-encore-mieux-sous-ubuntu-lucid-lynx-10-04/)

J’ai installé Ubuntu Lucid Lynx 10.04 hier sur mon [Acer Aspire One](http://doc.ubuntu-fr.org/acer_aspire_one) pour tester entre autre le support natif de l’iPhone d’Apple, puis qu’il s’agit d’une des nouveautés principal.

# Première connexion

La première fois que j’ai branché mon iPhone, je n’avais pas déverrouillé le téléphone, et Ubuntu avait bien monté un appareil photo nommé iPhone, sans photos dedans.

J’ai ré-itéré l’opération, mais cette fois le téléphone déverrouillé, et là, la magie a opéré! :-)

![iphone_10_04](/content/images/2017/08/iphone_10_04.png)

Les photos, cette fois sont accessibles (dommage qu’il n’y ai pas eut un message pour indiquer que l’iphone était verrouillé).

Pour ce qui est de l’icône supplémentaire, elle permet d’accéder au dossier `/private/var/mobile/Media` de l’iPhone qui est le dossier de base: Photos, vidéos, téléchargements, …

# Intégration dans Nautilus

Côté nautilus, il fait exactement ce que nous voulons :

![nautilus_iphone](/content/images/2017/08/nautilus_iphone.png)

Voici le petit bandeau qui apparaît lorsque l’on ouvre “iPhone de zedtux”.

# Musique vers l’iPhone

Pour commencer, j’ai lancé [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox), comme Ubuntu le proposait. L’iphone est bien là, je copie / colle un album sur le téléphone, et un transfert se fait, mais l’iphone ne fait pas de “Synchronisation en cours…” etc…
J’essaie tout de même de voire si l’iphone voie mes musiques, mais non… rien.

## Résoudre le problème de fuse

Après quelques recherches, je suis tombé sur le blog de FatButtLarry, qui [donne la solution](http://fatbuttlarry.blogspot.com/2010/01/ipod-touch-iphone-3g-ubuntu-910-in-5.html) !

Il suffit de lancer cette ligne de commande, qui vous demandera le nom de l’ipod/iphone.
Pour moi, j’ai répondu (sans les guillemets) “iPhone de zedtux”, comme c’est écrit sur le bureau.

    sudo adduser “$USER” fuse; echo -e “\n\nPlease type the name of your ipod:”; read ipod_name; mkdir -p “$HOME/.gvfs/$ipod_name/iTunes_Control/Device/”; ipod-read-sysinfo-extended sudo lsusb -v | grep 'iSerial' | awk 'length($0)>=68' | awk '{print $3}' “$HOME/.gvfs/$ipod_name/”

Maintenant, je ré-essaie, et ce coups ci, quand je copie depuis Rhythmebox, l’iphone fait bien une synchronisation après quelques secondes.

# Conclusion

Alors, pourquoi ce titre !!?

Si vous prenez 2 Mac, ou 2 PC avec Windows, qui utilisent iTunes et donc des librairies différentes, vous serez embêté puisque lorsque vous brancherez l’iphone et demanderez une synchronisation, tout ce qui ne correspond pas à l’existant sur la seconde machine sera simplement effacé.

Du coté d’Ubuntu, Rhythmebox va toujours aller chercher ce qui existe actuellement sur l’iPod ou l’iPhone, afin que lorsque vous copierez un album par exemple, il va juste l’ajouter aux autres albums, et du coup vous ne perdrez rien.

Donc, avec Ubuntu 10.04 et Rhythmebox, vous pouvez l’utiliser sur plusieurs machines utilisant Ubuntu 10.04, avec des librairies multimédia complètement différentes, sans jamais rien perdre !
D’autant plus que vous n’êtes pas limités aux 5 machines ayant l’autorisation d’utiliser votre téléphone !

(Merci a ma chérie pour la correction de fautes ;-))
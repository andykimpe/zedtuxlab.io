---
layout: post
title: Installer une version précise de Rubygems
date: '2015-01-19 15:14:15'
tags:
- docker
---

Si comme moi vous utilisez Docker, et que vous l'utilisez pour empaqueter une application Ruby On Rails qui utilise une version assez ancienne de Ruby, vous aurez sûrement besoin d'installer un Rubygems plus ancien afin d'éviter l'erreur:

    undefined method `source_index' for Gem:Module (NoMethodError)

Dans votre `Dockerfile` ajoutez une variable d'environment pour y spécifier la version que vous désirez utiliser:

    ENV RUBYGEMS_VERSION 1.6.2

Puis ajouter une commande `RUN` pour l'installer:

    RUN cd /tmp && \
      curl -L -O https://github.com/rubygems/rubygems/archive/v$RUBYGEMS_VERSION.tar.gz && \
      tar xzf /tmp/v$RUBYGEMS_VERSION.tar.gz -C /tmp && \
      cd /tmp/rubygems-$RUBYGEMS_VERSION/ && \
      ruby setup.rb && \
      rm -rf /tmp/rubygems-$RUBYGEMS_VERSION && \
      rm -rf /tmp/v$RUBYGEMS_VERSION.tar.gz

Et voilà ! :-)
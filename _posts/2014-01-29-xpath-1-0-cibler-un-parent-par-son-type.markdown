---
layout: post
title: XPath 1.0 cibler un parent par son type
date: '2014-01-29 15:50:35'
tags:
- xpath
---

Si vous lisez cet article c'est que probablement vous utilisez des XPaths pour écrire des tests d'intégration par exemple.

Si l'étape que vous implémentez est de cliquer un lien qui contient un text ou qui a un titre c'est très simple.

Mais disons que vous avez un ce code :

<script src="https://gist.github.com/zedtux/8690624.js"></script>

Ou encore ce code :

<script src="https://gist.github.com/zedtux/8690676.js"></script>

Et que vous désiriez cibler l'élément **a** avec un XPath.
C'est du coup plus difficile.

Voici la solution:

	//*[@title='Visit my page' or .='Visit my page']/ancestor::a[1]

**ancestor::a** veux dire que vous recherchons un élément du type lien hypertext (**a**) et le **1** veux dire le plus proche.
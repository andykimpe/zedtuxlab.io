---
layout: post
title: Mettre a jour Docker sur OSX
date: '2014-10-17 08:28:00'
tags:
- docker
---

Comme annoncé dans [mon article précédent](http://blog.zedroot.org/docker-1-3-0-will-be-released-soon/) la nouvelle version de Docker est sortie.

[Docker](https://www.docker.com) et [boot2docker](http://boot2docker.io) passent en version 1.3.0 et [fig](http://www.fig.sh) en version 1.0.0.

La procédure de mise à jour est relativement simple. Avec [Homebrew](http://brew.sh) vous allez pouvoir faire la mise à jour:

	$ brew update && brew upgrade
    ==> Upgrading 3 outdated packages, with result:
    boot2docker 1.3.0, docker 1.3.0, fig 1.0.0
    ==> Upgrading boot2docker
    ==> Installing dependencies for boot2docker: go, docker
    ==> Installing boot2docker dependency: go
    ==> Downloading https://downloads.sf.net/project/machomebrew/Bottles/go-1.3.3.yosemite.bottle.tar.gz
    ...
    ==> Summary
    🍺  /usr/local/Cellar/go/1.3.3: 4344 files, 114M
    ==> Installing boot2docker dependency: docker
    ==> Cloning https://github.com/docker/docker.git
    ...
      /usr/local/share/zsh/site-functions
    ==> Summary
    🍺  /usr/local/Cellar/docker/1.3.0: 9 files, 6.8M, built in 11 seconds
    ==> Installing boot2docker
    ==> Cloning https://github.com/boot2docker/boot2docker-cli.git
    ...
    ==> make goinstall
    🍺  /usr/local/Cellar/boot2docker/1.3.0: 2 files, 7.2M, built in 5 seconds
    ==> Upgrading fig
    ==> Downloading https://github.com/docker/fig/archive/1.0.0.tar.gz
    ...
    ==> python setup.py install --prefix=/usr/local/Cellar/fig/1.0.0
    🍺  /usr/local/Cellar/fig/1.0.0: 237 files, 3.3M, built in 6 seconds

Pour finir il faut mettre à jour l'image de boot2docker qui tourne dans VirtualBox sinon vous aurez la même erreur que je décris dans [mon article](http://blog.zedroot.org/error-response-from-daemon-client-and-server-dont-have-same-version/):

    $ boot2docker stop
    $ boot2docker download
    Latest release for boot2docker/boot2docker is v1.3.0
	Downloading boot2docker ISO image...
	Success: downloaded https://github.com/boot2docker/boot2docker/releases/download/v1.3.0/boot2docker.iso
	    to /Users/zedtux/.boot2docker/boot2docker.iso
    $ boot2docker up
    Waiting for VM and Docker daemon to start...
	................oooooooo
	Started.
	Writing /Users/zedtux/.boot2docker/certs/boot2docker-vm/ca.pem
	Writing /Users/zedtux/.boot2docker/certs/boot2docker-vm/cert.pem
	Writing /Users/zedtux/.boot2docker/certs/boot2docker-vm/key.pem
	
	To connect the Docker client to the Docker daemon, please set:
    export DOCKER_HOST=tcp://192.168.59.103:2376
    export DOCKER_CERT_PATH=/Users/zedtux/.boot2docker/certs/boot2docker-vm
    export DOCKER_TLS_VERIFY=1

Maintenant vous devriez avoir Docker 1.3.0 fonctionnel:

	Client version: 1.3.0
	Client API version: 1.15
	Go version (client): go1.3.3
	Git commit (client): c78088f
	OS/Arch (client): darwin/amd64
	Server version: 1.3.0
	Server API version: 1.15
	Go version (server): go1.3.3
	Git commit (server): c78088f
---
layout: post
title: 'D-Bus: Communication inter processus'
date: '2014-01-12 17:51:31'
tags:
- linux
---

![Logo freedesktop.org](/content/images/2017/08/freedesktop_logo.png)

D-Bus est un protocole de communication entre processus.

Il permet à un processus A, de publier ses méthodes aux autres applications, afin de pouvoir se faire contrôler par ce dernier.

## Cas concret

Voici un cas concret que je vient de repérer sur le [Brainstorm d’Ubuntu](http://brainstorm.ubuntu.com/): [Idée numéro 21189](http://brainstorm.ubuntu.com/idea/21189/).

L’idée, en résumé, est de pouvoir jouer une musique par Rhytmbox, le lecteur par défaut d’Ubuntu pour le moment, lorsque dans F-Spot, le gestionnaire de bibliothèque de photos, l’utilisateur lance un diaporama.

## Comment utiliser D-Bus pour contrôler RhytmBox

Rentrons dans le vif du sujet: Contrôler un processus, par D-Bus ! :)

#### Première étape: Lister les processus accessible.
Une bête commande vous permet de le faire : `qdbus`

    $ qdbus

    :1.0
    :1.1
    org.gnome.GConf
    :1.11
    org.gnome.keyring
    :1.12
    :1.158
    org.freedesktop.PowerManagement
    :1.19
    org.freedesktop.compiz
    :1.194
    :1.2
    org.gnome.SessionManager
    :1.202
    org.gnome.Rhythmbox

    …

**Rhytmbox** étant lancé sur ma machine à ce moment là, ont le retrouve dans cet extrait.

#### Deuxième étape: Lister les méthodes accessibles par D-Bus.

Là, aucune difficultés encore une fois.

Il vous suffit d’ajouter l’adresse du processus : `org.gnome.Rhythmbox`

    $ qdbus org.gnome.Rhythmbox
    /
    /org
    /org/gnome
    /org/gnome/Rhythmbox
    /org/gnome/Rhythmbox/DAAP
    /org/gnome/Rhythmbox/Player
    /org/gnome/Rhythmbox/PlaylistManager
    /org/gnome/Rhythmbox/Shell
    /org/gnome/Rhythmbox/Visualizer

Ce que vous voyez apparaître sont les différents modules offert par Rhytmbox.

Il faut donc en choisir une… Pour l’exemple, nous allons prendre le Player ;-)

    $ qdbus org.gnome.Rhythmbox /org/gnome/Rhythmbox/Player
    method QString org.freedesktop.DBus.Introspectable.Introspect()
    method QDBusVariant org.freedesktop.DBus.Properties.Get(QString interface, QString propname)
    method void org.freedesktop.DBus.Properties.Set(QString interface, QString propname, QDBusVariant value)
    signal void org.gnome.Rhythmbox.Player.elapsedChanged(uint)
    method uint org.gnome.Rhythmbox.Player.getElapsed()
    method bool org.gnome.Rhythmbox.Player.getMute()
    method bool org.gnome.Rhythmbox.Player.getPlaying()
    method QString org.gnome.Rhythmbox.Player.getPlayingUri()
    method double org.gnome.Rhythmbox.Player.getVolume()
    method void org.gnome.Rhythmbox.Player.next()
    method void org.gnome.Rhythmbox.Player.playPause(bool arg0)
    signal void org.gnome.Rhythmbox.Player.playingChanged(bool)
    signal void org.gnome.Rhythmbox.Player.playingSongPropertyChanged(QString, QString, QDBusVariant, QDBusVariant)
    signal void org.gnome.Rhythmbox.Player.playingUriChanged(QString)
    method void org.gnome.Rhythmbox.Player.previous()
    method void org.gnome.Rhythmbox.Player.setElapsed(uint elapsed)
    method void org.gnome.Rhythmbox.Player.setMute(bool mute)
    method void org.gnome.Rhythmbox.Player.setVolume(double volume)
    method void org.gnome.Rhythmbox.Player.setVolumeRelative(double volume)

    $

Voilà !

Vous avez tout les prototypes des méthodes publié par le processus !

## Finalement, comment exécuter une méthode ?

Encore une fois … rien de plus simple ! ;-)

    $ qdbus org.gnome.Rhythmbox /org/gnome/Rhythmbox/Player org.gnome.Rhythmbox.Player.getVolume
    1

La méthode me retourne la valeur du slide pour régler le volume.

Ici 1 veux dire “a fond !” ( La valeur minimal étant 0.0, la valeur maximal étant 1.0 ).

# Conclusion

Ici, je n’irai pas plus loin, c’est à dire côté programmation, mais je le ferai dans un prochain article ! :)

Donc, on peux voire que D-Bus est une solution très intéressante dans la programmation, car il permet de ne pas avoir à inclure des bibliothèques, et de développer une classe interface entre votre application et celle à contrôler par exemple !

Comme quoi… Linux.. c’est excellent ! ;-) 
---
layout: post
title: 'Lynis – Warning: found mail_name in SMTP banner, and/or mail_name contains
  ‘Postfix’.'
date: '2014-01-12 20:46:56'
tags:
- linux
- server
- security
---

If Lynis list the following warning:

> Warning: found mail_name in SMTP banner, and/or mail_name contains ‘Postfix’.

You will fix it by editing the file `/etc/postfix/main.cf` and removing everything after the ESMTP on the following line:
	
	smtpd_banner = $myhostname ESMTP $mail_name (Ubuntu)

And of course, restart postfix:

	sudo /etc/init.d/postfix restart
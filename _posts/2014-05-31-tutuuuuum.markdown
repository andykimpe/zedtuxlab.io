---
layout: post
title: Tutuuuuum
date: '2014-05-31 00:14:28'
tags:
- docker
---

![tutum-2](/content/images/2017/08/tutum-2.png)

A wired title to present you the very young, but with a lot of promises company [Tutum.co](http://www.tutum.co/) !

For your information, I'm not paid or working for Tutum.co.
I just want to share the beauty of their application because they're really working hard in order to provide a very easy and nice platform to easy the deployment of Docker containers.

In my opinion they are building the perfect platform for people looking for a fast, easy, and cheap solution !

# Overview

Tutum is a very young company, based in New York, composed of 7 employees, having released the first version of their platform in January 2014.

They are offering a very fast deployment solution (less than 10 seconds to deploy any containers) on their very cheap servers starting from $4 for a 256Mo of RAM and 0.25 CPU (EC2 Compute Unit) until the big 4GB of RAM and 4 CPU (EC2 Compute Unit) all running on SSD drives.

## Dashboard

![tutum_dashboard_docker](/content/images/2017/08/tutum_dashboard_docker.png)

Here is the most important part of Tutum. The dashboard !

Having a look to the screenshot should be enough to make you understanding what they're offering you !

Deploy, start/stop, destroy or scale very easily your Docker images in very few seconds.

![docker_monitoring_tutum](/content/images/2017/08/docker_monitoring_tutum.png)

Then, in case of an issue, they're providing for free, a **Crash recovery** feature which will try to restart your instances in order to keep your application running.

The killer feature is obviously the simple as slide and apply scaling option !
For the first time, you request more or less instances, click the **Apply** button, and just look at them booting !
And that-all-you-have-to-do !

## Monitoring

In order to know when to scale up or down, a monitoring screen has been implemented.

![tutum_monitor_docker-1](/content/images/2017/08/tutum_monitor_docker-1.png)

You will have quick look to the performances of your instances and can decide to increase of decrease the amount of instances.

## Jumpstarts

You need quickly a database ?

![tutum_jumpstarts](/content/images/2017/08/tutum_jumpstarts.png)

It's already ready. Simply select it and deploy it.

## Private images

Your application Docker image is hosted in a private repository ?
There is a screen for that too !

![tutum_import_image](/content/images/2017/08/tutum_import_image.png)

# Back to the example

Now that you have a good overview of Tutum.co, let's come back to the example.

Let's go and let's deploy brewformulas.org on Tutum.co !

The first good news is that when you'll create your account, Tutum will offer you $4 in order to try their service.
This is a first good sign of a nice service in my eyes.

One important point of deploying with Docker is the order of image deployment.
As Docker containers will be linked together in order to make them working, we have to deploy first the databases images so that when we will deploy the application containers, we will be asked to link to the existing and running databases containers.

## Databases

brewformulas.org is using 2 databases:

 - 1 postgres database for the website data
 - 1 redis database for the Sidekiq jobs

When logged in on [Tutum.co](http://www.tutum.co/), click the big **Launch new application** button.

Then here, from the Jumpstarts images, click the **Select** button of the `tutum/postgres` image.

![tutum_jumpstarts_postgresql](/content/images/2017/08/tutum_jumpstarts_postgresql.png)

You should keep the values for the **Application Name** and **Image Tag** fields.
Regarding the Container size field, I suggest you to select the **S** size minimum as the *XS* could be really slow and consuming the full memory quite fast.

**Number of containers** could remain to 1 but feel free to increase it as you'd like.

Now just click the **Launch** button and see the container booting :-)

Have a look to the **Logs** of the instance in order to have a look to the database password.

Repeat the exact same procedure for **Redis** using the `tutum/redis` image.

## Brewformulas.org woker image

Let's continue with the deployment of the worker image.

From the dashboard screen, click again on the **Launch new application** button and then select the **Private images** tab and then click **Add image**.

Now you have to fill in the 3 fields:

 - **Image Name** with quay.io/<username>/brewformulas.org
 - **Username**
 - **Password**

Then click the **Add image** button.

If everything goes well, you should see you image, and be able to click a **Select** button.

We are then back to the **Application configuration** screen.

Here you have to change the **Application Name** to **brewformulas-org-worker**.

Select the **worker** tag from the **Image Tag** field.

I suggest you to select the **S** size minimum as the *XS* could be really slow and consuming the full memory quite fast.

**Number of containers** could remain to 1 but feel free to increase it as you'd like.

Now you must click the **Next: environment variables** button in order to create the links.

![tutum_new_image_environment_variables](/content/images/2017/08/tutum_new_image_environment_variables.png)

In this screen, we will link together the databases images to the worker image so that it could fetch the data from the Postgres database container image, and the jobs from the Redis database image.
Click the **Link Applications** field and select the both databases containers.
Also click the **API roles** field and select the only **Full access** option.
Later some more API roles will be implemented.

As you can see, some additional environment variables have been added to the list, their are useful for the links in order to work.
But some of them are missing, like the `POSTGRESQL_USER` or `POSTGRESQL_PASSWORD`.

In order to remind you the `database.yml` details, let's have a quick look to it:

<script src="https://gist.github.com/zedtux/4957dd2a9698a9a45247.js"></script>

We are missing the following environment variables:

 - POSTGRESQL_USER
 - POSTGRESQL_PASSWORD
 - POSTGRESQL_TEMPLATE

POSTGRESQL_USER must contain the value **postgres**.

POSTGRESQL_PASSWORD must contain the value from the Docker container logs.

POSTGRESQL_TEMPLATE has to contain the value **template0** in order to avoid any encoding issues.

Regarding Redis, only the REDIS_PASSWORD environment variable has to be declared with the password from the Docker container instance **Logs**.
This is how I have named the variable in [config/appconfig.defaults.yml](https://github.com/zedtux/brewformulas.org/blob/master/config/appconfig.defaults.yml).

Finally click the **Launch** button.

## Brewformulas.org web image

Last step in order to have the application working is the deployment of the web image.

Redo the exact same operation as for the worker image, excepted for the **Image Tag** field where you have to select the **web** tag.

When the instance has started, and when you will go to the **brewformulas-org** instance details page, you should see the following:

![tutum_web_image_details](/content/images/2017/08/tutum_web_image_details.png)

Can you see the line http://brewformulas-org-zedtux.web.tutum.io/ ?
This is the load balanced URL for your application ! Yes ! [Tutum.co](http://www.tutum.co/) provide you the load balancer for free !

![docker_scaling_tutum](/content/images/2017/08/docker_scaling_tutum.png)

As of now, you are free to increase of decrease the amount of the Docker container instances within only 3 clicks.

# Conclusion

Tutum.co is an amazing platform for startups and for private use, but it could also be a very nice way for companies to host their applications.

The guys at Tutum.co are really nice (I know well as I'm testing a lot the platform :-)) and you will not loose your time to test their platform.

When you will fall in love with them, like me, don't forget to share on Twitter ! :-)
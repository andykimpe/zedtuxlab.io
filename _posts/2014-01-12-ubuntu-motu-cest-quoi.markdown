---
layout: post
title: 'Ubuntu MOTU : c’est quoi ?'
date: '2014-01-12 18:12:20'
tags:
- information
---

![Ubuntu MOTU](/content/images/2017/08/motu.png)

Lorsque vous vous plongerez (si ce n’est pas encore fais) plus en profondeur dans le monde d’Ubuntu, vous apprendrez qu’il existe une très grosse équipe nommé les MOTU.

Que veux dire MOTU ?

Voici la définition de MOTU:

**MOTU** sont les abréviations de **Masters Of The Univers**, en Français Les Maîtres de L’Univers… rien que ca !

Ne vous méprenez pas, ici Univers n’est pas l’univers tel que vous le connaissez ! ;-)

## Alors que font ces MOTU ?

Les MOTU se chargent de maintenir les paquets dans les dépôts Univers et Multivers d’Ubuntu.

Pour rappelle, ces dépôt contiennent tout les logiciels non supporté par Ubuntu, c’est à dire tout les logiciels où les gens de Canonical ne fournissent aucun support.

## Devenir un MOTU ?

Il faut donc avoir une très bonne connaissance de la création de paquet deb, pour pouvoir y participer.
Créer des paquet deb n’est pas dur en soit. Il faut pratiquer pas mal, tout comme la guitare ;)

Si vous êtes quelque peut intéressé, ne vous découragez pas en essayer d’imaginer la difficulté !

**Essayez avant de baisser les bras !**

## Informations

Je vous propose d’aller voire ce lien, qui vous expliquera en détails chaque étapes/grades : http://doc.ubuntu-fr.org/tutoriel/comment\_participer\_developpement\_ubuntu
---
layout: post
title: Développer en agile
date: '2014-01-12 20:07:02'
---

![AgileDilbert](/content/images/2017/08/AgileDilbert.jpg)

Ce mois-ci au boulot, il y a eu quelques petits changements dans les méthodes de travail : L’intégration de plus de [méthodes Agiles](http://fr.wikipedia.org/wiki/M%C3%A9thode_agile).

# Les méthodes avant
## Le Behavior Driven Development

Le [Behavior Driven Development](http://fr.wikipedia.org/wiki/Behavior_Driven_Development) ou **BDD**, est la première méthode Agile que j’ai apprise.

Cette méthode Agile est basée sur le [Test Driven Development](http://fr.wikipedia.org/wiki/Test_Driven_Development) ou **TDD**, qui, en résumé, consiste en l’écriture de tests avant celle du code, sauf qu’elle ajoute une couche supplémentaire aux tests.

Cette nouvelle couche est, elle, écrite dans la langue maternelle, normalement, ou une langue commune aux différents acteurs implémentant la nouvelle fonctionnalité. Le principe théorique est que tout le monde participe, c’est-à-dire le demandeur de la nouvelle fonctionnalité, le gestionnaire de projets, les développeurs, les testeurs, …
L’idée est que le demandeur de la nouvelle fonctionnalité et le gestionnaire de projet écrivent ensemble ce que l’on appelle une **feature**, contenant des scenarii décrivant la nouvelle fonctionnalité, qui eux-mêmes contiennent les différentes étapes (**steps**) que le demandeur de la nouvelle fonctionnalité effectue pour aboutir à sont besoin. (Quand je clique sur …  je dois voire un message …)

Une fois cette feature prête, il ne reste qu’à utiliser un programme prévu à cet effet, comme par exemple [cucumber](http://cukes.info/), ou encore [lettuce](http://lettuce.it/index.html), qui vont passer en vert les étapes valides, et en rouge les étapes ne fonctionnant pas. Du coup, votre mission sera de tout faire passer en vert. Donc comme pour le TDD, il faut écrire les étapes des scenarii avant de faire le code.

Cette méthode assure un niveau de qualité encore supérieur au TDD, et étant exécuté par le [serveur d’intégration continue](http://fr.wikipedia.org/wiki/Int%C3%A9gration_continue) à chaque commit, vous saurez très vite si vous venez de régresser dans votre code ! :-)

# Les méthodes après

## La méthode kanban

[La méthode kanban](http://fr.wikipedia.org/wiki/Kanban) est ultra simple, mais ultra efficace ~~surtout si elle est bien respectée~~!

Je ne vais pas donner d’explications, [car cet article de ce blog le fait extrêmement bien](http://www.openagile.net/2009/10/07/un-jour-au-pays-de-kanban/) (Merci et bravo à son auteur !)

Maintenant nous avons une vue claire de ce qui est à faire, ce qui se fait et par qui.
Nous voyons aussi où les demandes coincent…

Bref, une super méthode, même si elle n’est pas encore complètement mise en place…


## Le pair programming

La [programmation en binôme](http://fr.wikipedia.org/wiki/Programmation_en_bin%C3%B4me) est quelque chose que j’attendais depuis un moment ! C’est le changement le plus important de ce mois-ci !

Un collègue de boulot a déménagé à côté de moi, et nous pratiquons cette méthode seulement depuis 2 semaines, mais le résultat et les bienfaits apparaissent déjà ! Nous travaillons donc sur le même ordinateur en collaboration. Cette méthode n’a (à mes yeux) que des avantages pour le moment :

 - **Concentration maximale** : On passe moins de temps à glander, ou à s’auto-distraire et donc plus à coder. L’un l’autre ont se motive à bosser.
 - **Communication plus intense** : Nous discutons de comment corriger/implémenter telle ou telle chose de la meilleur façon possible.
 - **Apprentissage** : L’un apprend à l’autre et vice versa.

# Conclusion

Tout n’est pas fait dans les règles de l’art, certes, mais ces changements sont assez motivants, même si au final, pour le moment, le boulot, lui, reste trop monotone.
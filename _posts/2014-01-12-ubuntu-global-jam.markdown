---
layout: post
title: Ubuntu Global Jam !
date: '2014-01-12 18:18:07'
tags:
- ubuntu
---

![Ubuntu Global Jam](/content/images/2017/08/ugj09_button_brown_250x148_EN.png)

Les **Ubuntu Global Jam** sont un événement organisé régulièrement depuis peu, afin de travailler sur les bugs et maintenances divers d’Ubuntu.

L’idée principal est de **réunir physiquement les personnes**, afin de passer un moment **ensemble**. Puis c’est un moment pour travailler un peu.

Généralement, ce sont plein de petits bugs, qui sont fixé puis commité afin d’améliorer notre distribution préféré !
Le tout, dans la bonne humeur ! :-)

# Ubuntu Luxembourg

Et c’est là que j’introduis mon nouveau Grand projet: Reprendre le LoCo Luxembourgeois !

LoCo signifiant Local Community. (https://wiki.ubuntu.com/LoCoTeams)

Avec mon collègue et amis, dont le blog est dans ma Blogoliste, nous nous sommes apercu que depuis 2 ans, le LoCo Luxembourgeois avait disparut !

Nous avons vue là une excellente opportunité de montrer notre engagement envers Ubuntu !

Nous venons juste de prendre la décision de reprendre le flambeau ( depuis 1 semaine ), et nous commencons à peine à voire ce qu’il faut faire.

Donc, ce n’est pas encore pour demain ;)

Cela dit, nous sommes bien motivé par ce projet, et nous le mènerons à bien, j’en suis sûre !

## État actuel

Pour le moment, nous avons re-contacté les gens qui s’étaient inscrit à la mailing list ubuntu-lu, et à notre grande surprise, nous avons recu des réponses dès le lendemain !

Nous voulons, donc, participer au Ubuntu Global Jam en tant que ubuntu-lu afin de faire notre première présentation au monde Ubunterien !

## Le Futur

Nous envisageons de prendre en charge http://ubuntu-lu.org/ afin de réunir les Luxembourgeois autour d’Ubuntu.

## Nous contacter

Vous pouvez vous retrouver d’ores et déjà sur le salon IRC [#ubuntu-lu sur Freenode](irc://irc.freenode.net/ubuntu-lu).

Vous pouvez aussi nous contacter sur [la mailing list ubuntu-lu](https://lists.ubuntu.com/mailman/listinfo/Ubuntu-lu).
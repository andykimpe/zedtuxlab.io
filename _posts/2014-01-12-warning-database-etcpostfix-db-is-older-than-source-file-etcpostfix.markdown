---
layout: post
title: 'warning: database /etc/postfix/*.db is older than source file /etc/postfix/*'
date: '2014-01-12 18:15:04'
tags:
- linux
- maintenance
- server
---

Voici la solution à ce genre d’erreurs:

    Sep 21 17:32:59 r15868 postfix/trivial-rewrite[8704]: warning: database /etc/postfix/transport.db is older than source file /etc/postfix/transport

Il suffit de resynchroniser la base, avec postmap.

    cd /etc/postfix/
    postmap transport

(Ici je reprend l’erreur de l’exemple, mais ca peut-être virtual etc..)

C’est tout ! :-)
---
layout: post
title: Uninstall all the installed packages with Homebrew
date: '2014-10-19 09:17:11'
tags:
- homebrew
---

After having switched to Docker all my development environment, I've realized that still have running all the dependencies of my previous environment like for instance Postgres.

I have like 30 packages installed and it looks longer to me to remove them one-by-one than removing all and re-installing the 2 or 3 that I'd like to keep.

## Uninstall all the packages

The command is really simple:

    $ brew remove --force --ignore-dependencies $(brew list)

That's all folks. :)
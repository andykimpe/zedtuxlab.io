---
layout: post
title: Effacer toutes les images de Docker
date: '2014-05-17 08:48:39'
tags:
- docker
---

Docker n'est pas évident et donc au début on essaie et Docker a tendance a créer beaucoup d'images.

Après avoir compris le fonctionnement et fini vos tests, peut-être voudriez-vous nettoyer les images Docker et recommencer ?

Si c'est le cas, ouvrez un terminal et lancez la commande suivante:

	for i in `sudo docker images|awk '{print $3}'`;do sudo docker rmi -f $i;done

Cette commande effacera absolument toutes les images.


Inspiration: http://stackoverflow.com/questions/21398087/how-to-delete-dockers-images
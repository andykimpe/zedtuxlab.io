---
layout: post
title: 'Python: Encoder en base64 un string'
date: '2014-01-12 17:45:36'
tags:
- python
---

![Python](/content/images/2017/08/python-1.png)

Je vient de tomber sur un blog où la personne donne son adresse email, via une petite ligne de code, en [python](http://www.python.org/).

J’ai tout de suite essayé avec mon adresse .. et ca marche parfaitement !!*

Faisons un petit exemple ! :)

Prenom l’email helloworld@gnu.org:

    $ python -c "print 'helloworld@gnu.org'.encode('base64')"
    aGVsbG93b3JsZEBnbnUub3Jn

Maintenant, on rédige la ligne qui permet de faire la conversion inverse :

    python -c "print 'aGVsbG93b3JsZEBnbnUub3Jn'.decode('base64')"

Essayez le !! :) 
---
layout: post
title: Le développement en mode “non-blocking”
date: '2014-01-13 13:06:07'
tags:
- nodejs
---

Je connaissais déjà un petit peu [node.js](http://nodejs.org/) ([voir la description sur Wikipedia](http://fr.wikipedia.org/wiki/Node.js)), mais j’ai approfondis récemment mes connaissances grâce à [code school](https://www.codeschool.com/).
Ils ont un cour très bien fait, que vous pourrez trouver [à cette adresse](https://www.codeschool.com/courses/real-time-web-with-nodejs), qui vous permettra de comprendre le fonctionnement et pourquoi le développement évènementiel, sans blocages, peux rendre une application plus rapide. Mais attention, c’est en Anglais.

Cela dit, si vous ne connaissez pas [Javascript](http://fr.wikipedia.org/wiki/JavaScript), vous devrez commencer par le cour [JavaScript Road Trip Part 1](https://www.codeschool.com/courses/javascript-road-trip-part-1) (**Gratuit mais en Anglais**), puis continuer avec les autres cours sur le Javascript.

## Simple explication de Node.js

Node.js, créé par Ryan Dahl en 2009, permet d’écrire des applications réseau évènementiel, en utilisant le langage Javascript.

Son principe de fonctionnement est de faire un appelle à une méthode **A**, qui prendra en dernier argument un “callback”, qui est ni plus ni moins le nom d’une fonction **B**, qui devra être appelée dés que la fonction **A** finira son travail.

Cela permet donc, en attendant, de faire autre chose. Une très exemple serait celui d’une personne qui envoie une vidéo vers une application node.js, qui va convertir cette vidéo au fur et à mesure qu’elle reçoit les morceaux de la vidéo. L’application finira donc quand elle recevra le dernier morceau de la vidéo et qu’elle l’aura convertit. Dans une application traditionnelle, donc dite bloquante, elle attendrait d’avoir reçu tout les morceaux de la vidéo, avant de commencer le conversion, et donc finir plus tard au final.

## Une idée de programme en Node.js

Bien entendu, une fois le cour terminé, la première chose que j’ai fait est de trouver une idée d’application évènementiel que je pourrais réaliser.

Cette première idée, pour vous faire deviner, pourrait s’appeler **apt-get.js** ! Pour ceux qui ne connaissent pas apt-get, c’est une ligne de commande [apt](http://fr.wikipedia.org/wiki/Advanced_Packaging_Tool) pour mettre à jour ou installer des programmes sur une distribution basé sur [Debian](http://fr.wikipedia.org/wiki/Debian_GNU/Linux), comme [Ubuntu](http://www.ubuntu.com/).

Lorsque vous lancez la commande `apt-get update`, par exemple, l’application va tout d’abord télécharger chaque fichiers de chaque dépôts, et ne faire que cela, puis seulement lorsque le dernier serveur est contacté et a répondu, alors l’application va commencer à traiter chaque fichier.

Même comportement avec `apt-get upgrade`, l’application télécharge d’abord tout les paquets avant de commencer l’installation. Par exemple si je veux installer un paquet qui a disons 15 dépendances, donc 16 paquets a télécharger et a installer, alors je vais devoir attendre le téléchargement des 16 paquet, puis seulement l’installation débutera, en commencent bien entendu par les dépendances, pour finir par le paquet demandé.

En Node.js, chaque téléchargements se feraient simultanément. Dés quel le téléchargement d’un paquet serait terminé, et que chaque une de ses dépendances seraient installées, il pourrait être installé à sont tour, alors que les autres téléchargements continueraient.

La durée de la mise jour serait égale au temps de téléchargement du plus gros paquet, et de sont installation (comprenant donc ses dépendances).

## Conclusion

En conclusion il n’est pas simple de comprendre le fonctionnement ou même l’intérêt tout de suite, mais une fois compris, cela nous donne envie de faire du développement évènementiel partout!

Cela dit attention! Une autre idée m’étais venu: ré-écrire les lignes de commandes utilisé tout les jours en node.js. Par exemple imaginez la commande ls (lister les fichiers d’un chemin), dans un répertoire contenant des milliers de fichiers, et qui afficherai au fur et à mesure le nom des fichiers trouvés sans attendre que tous soient retrouvés ? Plus besoin d’attendre… Mais Node.js n’est pas fait pour ça!
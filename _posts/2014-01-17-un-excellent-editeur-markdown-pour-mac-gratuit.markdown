---
layout: post
title: Un excellent éditeur Markdown pour Mac gratuit
date: '2014-01-17 10:14:09'
---

![mou_256x256-1](/content/images/2017/08/mou_256x256-1.png)

Je cherchais un éditeur Markdown sur Mac qui soit simple, avec un rendu en temps réel, beau et gratuit.

J'ai trouvé **Mou**.

![](http://25.io/mou/img/1.png)

Je vous le conseille vivement! Vous pourrez le télécharger gratuitement sur [le site officiel](http://mouapp.com/).

Maintenant je doit trouver un tel logiciel pour Linux. :-)
---
layout: post
title: 'Douane: Release date announcement and status'
date: '2014-01-13 12:52:22'
tags:
- my-developments
- douane
---

I didn’t updated my blog since [my latest article](/douane-first-screenshot-of-the-firewall-configurator/) from the 14th of January because I worked a lot on my **Douane** project and also spent time to live :-)

## Current status

The main behavior to block a new application and ask to the user is he/she allow/deny it to connect to Internet is fully implemented!

Also another good news is that I have a release date! :)

Since 2 weeks I was having a close look at my progress to estimate if the first release of Douane could be finished for end of February and it looks good!

I already have a colleague from work that want to test it in order to help me. As soon as he will validate my work, I will submit my app to the [Canonical](http://www.canonical.com/) team that is responsible to review it.

 

So here is the list of open point before to release testing version:

 - Implement the save of Firewall rules into a file
 - Crypt that file to protect you against bad things
 - Implement application executable hashing (Until now I’m identifying it only by its path.)
 - Implement license mechanism to sell my application
 - Improve Question Window design
 - Finalize the configuration application
 - Fix all memory leaks (using [valgrind](http://valgrind.org/) and [kedr](http://kedr.berlios.de/))
 

## Something to show us?

I will show you how looks like the Question Window as I named it, but keep in mind that is a first version and will evaluate.

![Screenshot_from_2013_02_04_201718](/content/images/2017/08/Screenshot_from_2013_02_04_201718.png)

As you can see I’m using a Notebook in order to have only one single window to not popup a lot of them that will boring you.

This window appear only when needed, and if you close it, it will re-appear as soon as an unknown application try to connect again.

Without taking care of the style, let me know what you think about it and if you have any suggestion.
---
layout: post
title: 'Ruby: Désinstaller tout les gems'
date: '2014-01-12 19:44:26'
tags:
- ruby
---

Si vous avez besoin de désinstaller tout vos gems sans exception, il suffit de lancer cette commande !

Attention ! Irréversible !

	gem list | cut -d” ” -f1 | xargs gem uninstall -aIx
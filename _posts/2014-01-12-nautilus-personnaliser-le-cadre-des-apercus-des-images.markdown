---
layout: post
title: 'Nautilus: Personnaliser le cadre des apercus des images'
date: '2014-01-12 18:24:17'
tags:
- linux
- gnome
---

Actuellement, dans la version 2.26 par exemple de [GNOME](http://www.gnome.org/), si vous ouvrez un dossier avec une photo ou une icone, vous avez ceci :

![Nautilus thumbnails border normal](/content/images/2017/08/nautilus_thumbnails_border_normal.png)

Voyons un peu comment modifier ce cadre voire même le supprimer ! :-)

## Personnaliser le cadre

Il pourrait être imaginé de modifier ce cadre dans un thème pour GNOME.
(Jamais vue encore ! :))

Pour personnaliser ce cadre, il vous suffit de modifier le fichier `/usr/share/pixmaps/nautilus/thumbnail_frame.png` !

Voici un petit exemple de ce que ca donne :

![Nautilus thumbnails border custom](/content/images/2017/08/nautilus_thumbnails_border_custom.png)

## Supprimer le cadre

Là rien de bien difficile ! :)
Il suffit de faire une image vide… C’est tout ! ;)

Ce qui nous donne donc :

![Nautilus thumbnails no border](/content/images/2017/08/nautilus_thumbnails_border_without.png)

Personnellement je préfère sans cadre ;-)
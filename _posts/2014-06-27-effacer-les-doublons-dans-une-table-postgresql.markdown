---
layout: post
title: Effacer les doublons dans une table PostgreSQL
date: '2014-06-27 09:41:37'
tags:
- sql
- postgres
---

Effacer les doublons dans une table n'est pas une tâche très souvent faite, ce qui fait que l'on oublie facilement (du moins, c'est mon cas).

Cet article est juste un moyen de me le rappeler :-)

Disons que les doublons de la table **users** soient identifiés avec les colonnes **name**, **parent\_id** et **country\_id**, voici la requête SQL à exécuter:

	DELETE FROM users WHERE id NOT IN (
      SELECT max(id) FROM users
      GROUP BY name, parent_id, country_id
    )

La requête recherche l'**id** le plus haut parmis les entrées de la table qui ont les même valeurs dans les champs spécifiés.
Puis tout les entrées qui n'ont pas cet id sont supprimé.
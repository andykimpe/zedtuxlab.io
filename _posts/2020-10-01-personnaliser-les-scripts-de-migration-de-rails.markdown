---
layout: post
title: 'Personnaliser les scripts de migrations de Rails'
date: '2020-10-01 12:47:00'
tags:
- rails
---

Ruby on Rails vient avec en ensemble de générateurs afin de vous aider à développer votre application.

L'un des plus utilisé, j'imagine, c'est bien `migration` qui permet de créer des scripts de migration de votre base de données:

```
$ bundle exec rails g migration create_users email:string:uniq
      invoke  active_record
      create    db/migrate/20201001105449_create_users.rb
```

Une chose que Rails vous permet de faire est de personnaliser le patron utilisé pour générer ce fichier en placant, tout simplement, un nouveau patron au bon endroit dans votre projet.

Je l'ai découvert dans [ce _Pull Request_](https://github.com/rails/rails/pull/13972) et j'ai trouvé une confirmation dans [cette réponse sur StackOverflow](https://stackoverflow.com/a/21641311/1996540).

Il y a 2 patrons utilisés pour générer ces scripts :

* create_table_migration.rb.tt : Lorsque vous créez une nouvelle table (comme dans mon exemple plus haut)
* migration.rb.tt : Lorsque vous désirez change l'existant

Créez le dossier `lib/templates/active_record/migration/` dans votre projet puis téléchargez ces deux fichiers depuis les sources de Rails (selon votre version):

```
RAILS_VERSION=$(bundle exec rails --version | awk '{print $2}')
curl https://github.com/rails/rails/blob/v$RAILS_VERSION/activerecord/lib/rails/generators/active_record/migration/templates/create_table_migration.rb.tt --output lib/templates/active_record/migration/create_table_migration.rb.tt
curl https://github.com/rails/rails/blob/v$RAILS_VERSION/activerecord/lib/rails/generators/active_record/migration/templates/migration.rb.tt --output lib/templates/active_record/migration/migration.rb.tt
```

_**ATTENTION:** Si votre version de Rails est inférieur à la version 5.2.0, vous devez retirer `.tt` à la fin du nom des fichiers._

Vous pouvez maintenant modifier ces fichiers puis lancer le générateur pour tester vos modifications 🎉.

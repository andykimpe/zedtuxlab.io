---
layout: post
title: 'Octomore 2: Le Whisky le plus tourbé au monde !'
date: '2014-01-12 19:04:54'
---

![octomore_2_140](/content/images/2017/08/octomore_2_140.jpg)

Voici la folie que je me suis faite ce week-end: Un Octomore 2 à 140 ppm (part per million) !

Le fais que ce soit une bouteille de collection ne m’intéresse pas le moindre du monde (désolé pour les collectionneurs), par contre, ce qui est dedans est d’une saveur à faire dresser le poile sur les bras !

En effet, voici le Whisky le plus tourbé au monde ! 140 ppm de phénol !
Les PPM est l’ “unité de valeur servant à mesurer le degré de tourbage” (citation du site http://www.territoirewhisky.fr/lexiqueWhisky.html)
Pour vous rendre compte, encore une citation (http://www.whisky-news.com/Fr/report/Peat\_ppm\_Fr.pdf).

> Les whiskies très tourbés sont faits à partir de malt avec plus de 30 ppm de phénols, les moyennement tourbés d’environ 20 ppm et les légèrement tourbés sont généralement en dessous de 15 ppm. Même le malt “non-tourbé”, contient des phénols, mais à des niveaux très base, entre 0.5 et 3 ppm.

Au final j’adore ce Whisky, et heureusement vue le prix, mais il est clair qu’il sera bue lentement !
D’ailleurs, il faut savoir qu’un font de verre suffit bien assez ! Comparé aux autres Whisky plus commerciale où un demi verre n’est pas effrayant !

Je voulais partager ca avec qui veux bien le lire ! ;) 
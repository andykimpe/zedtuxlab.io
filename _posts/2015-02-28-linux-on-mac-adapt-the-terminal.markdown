---
layout: post
title: 'Linux On Mac: Adapt the terminal'
date: '2015-02-28 18:22:10'
tags:
- linux-on-mac
---

A feature I was using a lot with the OS X terminal (Well iterm2 actually) is the cleaning of the terminal with `CMD + k`.
The Debian terminal can do the same but needs to be configured.

Open the terminal preferences and re-define the shortcut as desired.
In my case I wanted to keep the same (`CMD + k`):

![debian-terminal_preferences_shortcuts-reset-and-clear](/content/images/2017/08/debian-terminal_preferences_shortcuts-reset-and-clear.png)

*Notice the Reset and Clear action*

I recommand you to have a look to [my article where I'm explaining how to change switch the CTRL and CMD command](http://blog.zedroot.org/linux-on-mac-use-cmd-instead-of-ctrl/).
---
layout: post
title: 'syslog: Log en réseau !'
date: '2014-01-12 13:14:00'
tags:
- linux
- server
- syslog
---

![Log viewer](/content/images/2017/08/logviewer.png)

Récemment j’ai eut à trouver une solution **propre** pour surveiller des emails envoyé par [postfix](http://www.postfix.org/), et récupérer leur statue.

C’est alors que j’ai creusé en profondeur `syslog` et que j’ai trouvé mon bonheur ! :D

## Le but à atteindre

Donc postfix tourne sur une machine, mon projet sur une autre machine.
Il faut envoyer un email par ce serveur postfix, et retrouver si l’email est bien arrivé chez le destinataire, ou si au contraire il s’est viandé lamentablement.

## Le rôle de syslog

postfix, comme énormément d’application sous Linux, utilisent `syslog` pour logger ce qui se passe. (créer un fichier de log dans `/var/log/`)
Donc, syslog va, après avoir poster le log dans le fichier de log, transmettre cette ligne de log à la machine où tourne le projet.
Et pour faire ca, rien de plus simple !!! :-)

## Configurer syslog sur la machine où tourne postfix

Il faut préciser à syslog qu’il doit émettre les logs à une machine, et surtout… quelle machine ! :-)
Pour faire ca, il faut juste modifier le fichier `/etc/syslog.conf` et créer un régle:
(A faire n’importe où dans le fichier)

    mail.* @machine_distante

Puis, pour finir, redémarrer syslog:

    sudo /etc/init.d/syslog restart

## Configurer syslog sur la machine distante

Là c’est encore plus simple ! ;-)

Il faut juste que `syslog` démarre avec l’argument “-r” !
Pour faire ca, il faut modifier le fichier `/etc/default/syslogd` :

    #
    # Top configuration file for syslogd
    #

    #
    # Full documentation of possible arguments are found in the manpage
    # syslogd(8).
    #

    #
    # For remote UDP logging use SYSLOGD=”-r”
    #
    SYSLOGD=”-r”

Une fois de plus, il faut redémarrer syslog:

    sudo /etc/init.d/syslog restart

Et voila !!! :-)

## Résultat

Maintenant, quand un mail est traité, les logs mail seront écrit dans le `/var/log/` de la machine où tourne l'application and elle peut donc les "lires" !!
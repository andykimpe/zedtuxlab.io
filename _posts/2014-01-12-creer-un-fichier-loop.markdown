---
layout: post
title: Créer un fichier Loop
date: '2014-01-12 13:07:56'
tags:
- linux
- command-line
---

## Qu’est ce qu’un fichier loop ?

C’est un fichier généré, depuis une loop, une boucle, avec un source, et une destination.
Une des utilité de ce genre de commande, est la création d’un fichier, qui servira d’espace de stockage (crypté par exemple).
Donc, ont créer, par exemple, un fichier de 100Mo, que l’on crypte, et que l’on place sur une clé USB, pourquoi pas, puis, il suffit d’y accéder en tapant la passphrase, et vous pourrez y déposer des fichiers qui seront crypté une fois que vous les copierez à l’intérieur.

Moi j’en ai juste besoin pour tester mon programme. Donc je ne décris que la partie qui m’intéresse: Générer un fichier.

## Comment ?

La commande dd va créer le fichier, et losetup va permettre d’utiliser ce fichier.

Il suffit d’utiliser une ligne de commande que voici:

    dd if=ici_la_source of=ici_la_destination bs=taille_du_buffer count=nombre_de_fois_que_le_buffer_est_copié

Concrètement, ca pourrait être:

    dd if=/dev/zero of=/home/zedtux/privatespace bs=1M count=100

Ici, nous utilisons comme source /dev/zero (**if**), ce qui produira un fichier vide, dont on prend 1Mo (**bs**), que l’on répète 100 fois (**count**), dans le fichier `/home/zedtux/privatespace` (**of**)
Donc, nous aurons un nouveau fichier `/home/zedtux/privatespace` de 100Mo.

## Y mettre des données aléatoire

Toujours dans l’optique de tester mon programme, j’ai besoin que ce fichier contienne des données, mais je m’en fou de ce que c’est.
Et là, Linux, encore une fois, à une solution !! :-)

Comme toujours, une petite ligne de commande, et c’est réglé !!

Pour commencer, je monte le fichier sur une loop:

    losetup /dev/loop0 /home/zedtux/privatespace

Maintenant, je peux injecter des données aléatoire:

    badblocks -s -w -t random -v /dev/loop0

Source: http://goohackle.com/how-to-create-a-portable-encrypted-file_system-on-a-loop-file/
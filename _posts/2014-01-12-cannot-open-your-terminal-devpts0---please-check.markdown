---
layout: post
title: Cannot open your terminal ‘/dev/pts/0′ – please check.
date: '2014-01-12 17:36:53'
tags:
- linux
- command-line
---

Si vous lisez cet article, c’est que vous “jouez” avec `screen` et que vous désiez ouvrir un écran, mais vous vous tapez cette erreur ?

Pas de soucis ! ;-)

Il vous suffit de donner des droits (temporairement … hun ?) avec la commande :

    chmod go+rw /dev/pts/0

Et voilà ! Plus d’erreur !

Pour revenir aux permissions de départ :

    chmod 620 /dev/pts/0

---
layout: post
title: Bose QuietComfort 35 et Ubuntu/ElementaryOS
date: '2020-02-03 08:50:47'
tags:
- linux
---

Petite note pour faire fonctionner correctement le casque de Bose sous Linux, et plus particulièrement sous Elementary OS (5.1).

L'association du casque avec Elementary OS fonctionne parfaitement sans rien installer de particulier.

Cependant le casque réagit bizarrement en passant entre les profiles [HDP](https://en.wikipedia.org/wiki/List_of_Bluetooth_profiles#Health_Device_Profile_(HDP)) et [HFP](https://en.wikipedia.org/wiki/List_of_Bluetooth_profiles#Hands-Free_Profile_(HFP)) alors que j'utilises Spotify, qui n'utilise donc pas le micro, il n'y a donc pas de raisons de passer à ces profiles.

Le profile bluethooth idéal est l'[A2DP](https://en.wikipedia.org/wiki/List_of_Bluetooth_profiles#Advanced_Audio_Distribution_Profile_(A2DP)) qui permet d'avoir une haute fidélité du son, mais aussi les commandes du casque qui fonctionnent (Lecture/pause, augmenter/réduire le volume).

Le premier point que je voulais résoudre est que dans la liste des périphériques bluethooth j'avais mon casque en double, dont une ligne grisée que je ne pouvais pas séléctionner, et donc que je ne pouvais pas supprimer.

J'ai trouvé comment l'effacer grâce à [cette réponse de Stack Overflow](https://askubuntu.com/a/884924/149516):

```
# bluetoothctl va lister tous les périphériques bluethooth avec leur adresse
# physique necessaire pour la commande suivante
bluetoothctl

# Copier/coller l'adresse physique
remove aa:bb:cc:dd:ee:ff
```

Puis j'ai recommencé la procédure d'association du casque avec Elementary OS.

Ensuite, pour régler le problème du profile, dans les paramètres système je n'ai pas le menu déroulant pour choisir le profile de sortie du casque, mais j'ai trouvé [cette autre réponse Stack Overflow](https://askubuntu.com/a/1020015/149516) qui permet de le faire manuellement en ligne de commande:

```
# Cette commande liste les sorties actives 
pacmd list-cards
2 card(s) available.
    index: 0
	name: <alsa_card.pci-0000_00_1f.3>
    ...
 
    index: 2
	  name: <bluez_card.4C_87_5D_06_77_6A>
	  driver: <module-bluez5-device.c>
	  owner module: 24
	  properties:
		device.description = "Casque de Guillaume"
		device.string = "4C:87:5D:06:77:6A"
		...
	  profiles:
		a2dp_sink: High Fidelity Playback (A2DP Sink) (priority 40, available: unknown)
		a2dp_source: High Fidelity Capture (A2DP Source) (priority 20, available: no)
		headset_head_unit: Headset Head Unit (HSP/HFP) (priority 30, available: unknown)
		off: Off (priority 0, available: yes)
      active profile: <headset_head_unit>
      ...

# Mon casque est donc en position 2 et utilise le profile "Headset Head Unit (HSP/HFP)" que je dois changer en "High Fidelity Playback (A2DP Sink)":
pacmd set-card-profile 2 a2dp_sink
```

Voilà le son est enfin correct!
---
layout: post
title: APT Mettre tout à jour sauf un paquet
date: '2016-02-25 06:45:57'
tags:
- apt
- debian
---

J'ai rencontré quelques soucis avec la mise à jour de Docker engine 1.10.

Mais je désire mettre à jour les autres paquets.

#### Mettre en attente un paquet

Avec `apt`, le gestionnaire de paquets de Debian/Ubuntu, il est possible de mettre un paquet en attente (`on hold`).

Pour ce faire :

    $ sudo apt-mark hold docker-engine
    docker-engine set on hold.

Maintenant si je fais un `apt-get upgrade`, le paquet est bien ignoré.

#### Retirer d'attente le paquet

Plus tard, lorsque je serai prêt à examiner le soucis, pour pouvoir mettre à jour `docker-engine`, je ferai:

    $ sudo apt-mark unhold docker-engine
    Canceled hold on docker-engine.

Maintenant le paquet est disponible dans `apt-get upgrade`.
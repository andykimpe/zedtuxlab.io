---
layout: post
title: OSX ssh-copy-id
date: '2014-09-18 12:50:12'
tags:
- server
- ssh
---

In the case you're using OSX and you would like to use the `ssh-copy-id` command in order to import your SSH key to a server (which avoid the server to ask your user password), then you can install it really quickly thanks to Homebrew:

    $ brew install ssh-copy-id

Then just do something like the following:

    $ ssh-copy-id -i ~/.ssh/id_rsa.pub zedtux@zedroot.org
    ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
	ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
    
    Number of key(s) added:        1
	
	Now try logging into the machine, with:   "ssh 'guillaumeh@192.168.97.101'"
	and check to make sure that only the key(s) you wanted were added.

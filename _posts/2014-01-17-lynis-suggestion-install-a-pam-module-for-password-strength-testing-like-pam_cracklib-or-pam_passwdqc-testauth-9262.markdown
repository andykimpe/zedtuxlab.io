---
layout: post
title: 'Lynis: Suggestion: Install a PAM module for password strength testing like
  pam_cracklib or pam_passwdqc [test:AUTH-9262]'
date: '2014-01-17 18:21:41'
tags:
- server
- security
---

If Lynis list the following suggestion:

> Suggestion: Install a PAM module for password strength testing like pam_cracklib or pam_passwdqc [test:AUTH-9262]

Then you have to install of the 2 libraries. I have installed the first one on my Ubuntu server:

    sudo apt-get install libpam-cracklib

But then Lynis was still complaining. This is a small bug because in the case Lynis find the path `/lib/security/` then he expect to find the **.so** file in this path. So in order to fix it:

    sudo ln -s /lib/x86_64-linux-gnu/security/pam_cracklib.so /lib/security

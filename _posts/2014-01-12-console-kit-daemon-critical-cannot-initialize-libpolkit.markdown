---
layout: post
title: 'console-kit-daemon: CRITICAL: cannot initialize libpolkit'
date: '2014-01-12 12:29:02'
tags:
- linux
- maintenance
- server
---

Ce matin, ~~un lapin~~, je me suis aperçu que mon serveur, ne répondait plus…

Plus de web, ne réponds plus au `ping`, et plus de `ssh`… reboot forcé obligé.

Une fois le serveur rebooté, tout re-fonctionne parfaitement.
Un petit tour dans les logs… et j’ai trouvé deux choses bizarres.
La première est ce message d’erreur :

    May 8 20:11:01 r15868 console-kit-daemon[312]: CRITICAL: cannot initialize libpolkit
    May 8 20:12:01 r15868 console-kit-daemon[421]: CRITICAL: cannot initialize libpolkit
    May 8 20:13:02 r15868 console-kit-daemon[519]: CRITICAL: cannot initialize libpolkit
    May 8 20:14:01 r15868 console-kit-daemon[617]: CRITICAL: cannot initialize libpolkit
    May 8 20:15:01 r15868 console-kit-daemon[718]: CRITICAL: cannot initialize libpolkit

Vu que ça se répète régulièrement … sans trop savoir à quoi sert ce console-kit-daemon, je vais tout même résoudre cette erreur.

Pour ce faire, un petit :

    sudo apt-get install policykit


Voilà, les logs sont clean maintenant.

L’autre problème que j’ai observé, pourrait être l’origine du plantage … peut-être.
En gros, mon serveur a planté le 8 Mai, à 20h15 et 3 secondes. ( Dernière ligne de log, relevé dans auth.log )

J’ai trouvé cette ligne, dans le syslog:

    May 8 20:15:01 r15868 /USR/SBIN/CRON[722]: (root) CMD (/usr/local/rtm/bin/rtm 38 > /dev/null 2> /dev/null)

2 secondes après cette ligne, le serveur a planté…
Donc … à étudier !

**Edit:** Pour le moment, le serveur est de nouveau stable !
Donc je suppose que s’était le bien le problème.
---
layout: post
title: 'SSH: bad ownership or modes for chroot directory'
date: '2014-01-12 19:43:29'
tags:
- linux
- maintenance
- server
---

![Terminal_128x128-1](/content/images/2017/08/Terminal_128x128-1.png)

Si vous donnez à un utilisateur un dossier home partagé (le dossier appartient à un groupe, et plusieurs utilisateurs peuvent y accéder), et que vous avez le message **bad ownership or modes for chroot directory** dans les logs de Linux, c’est que, comme dis dans le message, le dossier home n’a pas les bon droits.

## Fixer le problème

Pour réparer ce petit soucis, il faut que le propriétaire du dossier (owner) soit root, et que le groupe du dossier soit le groupe que vous voulez utiliser pour liés vos utilisateurs à ce dossier.

Et le tout avec des permissions à 775.

## Un exemple

Pour que ce soit plus clair:

Disons que le groupe que vous avez créé pour relier vos utilisateur soit uploaders, et vous voulez donc que les utilisateurs john, jean, et robert puissent envoyer des fichiers dans le dossier `/home/uploads`.

Les utilisateurs seront donc créé avec la commande suivante:

    useradd -b /home/uploads -d /home/uploads -G uploaders -r john

Ensuite vous n’avez qu’a créer le dossier `/home/uploads` et à lancer ces commandes:

    sudo chmod 775 /home/uploads
    sudo chown root:uploaders /home/uploads

Et voilà !!! :) 
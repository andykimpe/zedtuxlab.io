---
layout: post
title: Les clones de Github
date: '2014-02-10 23:14:14'
tags:
- git
---

![git_logo](/content/images/2017/08/git_logo.png)

Dans certains cas vous ne pouvez pas utiliser [Github](https://github.com/) (ou encore [BitBucket](https://bitbucket.org/)) pour héberger les code sources de vos projets.

Alors vous pouvez utiliser un clone.

## GitBlit

![gitblit](/content/images/2017/08/gitblit.png)

Gitblit est entièrement écrit en Java. Selon moi le visuel laisse vraiment a désirer mais semble fonctionnel.

Site web http://gitblit.com/.

## GitBucket

![gitbucket](/content/images/2017/08/gitbucket.png)

GitBucket est écrit en Scala (JVM donc) et ressemble comme deux gouttes d'eau à Github.
C'est beaucoup mieux que GitBlit, toujours selon moi.

## GitLab

![gitlab](/content/images/2017/08/gitlab.png)

GitLab, quand à lui, est écrit en Ruby et est un clone mais avec des personnalisations qui le rend, encore une fois selon moi, plus attractif.

## Conclusion

Cet article est une simple comparaison visuel des outils que j'ai trouvé ici et là.
**Rien ne vaux un réel essaie.**

Au boulot nous utilisons Gitlab, et nous avons pas mal de projets, sans problèmes de performance (Java est plus rapide que Ruby) donc le visuel est, dans ce cas, une part importante du choix :-)
---
layout: post
title: 'Bash: Commande Screen'
date: '2014-01-12 17:40:19'
tags:
- linux
- command-line
---

![Screens](/content/images/2017/08/gnome_netstatus_idle.png)

La commande `screen` de Linux, permet de lancer dans sessions dans des terminaux virtuel tel que les tty.

Ça peut permettre de “deamoniser” un programme en ligne de commande par exemple ;-)
(C’est à dire faire tourner en tache de font une application)

C’est bien beau, mais pour vérifier que tout fonctionne, il faudrait pouvoir accéder à cet écran, afin de voire et inter-agir sur le programme.

Ou tout simplement, si vous êtes administrateur, et que vous devez prendre le contrôle d’un écran d’un utilisateur.

## Lister les écrans d’un utilisateur

Avant de se connecter, il faut savoir où se connecter ! ;-)

Pour se faire, nous allons utiliser ls, tout simplement, car le dossier `/var/run/screen/` contient un dossier par utilisateur, et ce dossier contient un fichier de session.

    ls -al /var/run/screen/S-nomutilisateur/

Les fichiers commencent par le PID du programme lancé sur la machine, puis le nom de l’utilisateur.

Par exemple, si l’utilisateur qui lance le programme **application1** qui a le **PID 20160**, est **utilisateur1**, le fichier sera 20160.utilisateur1.

## Se connecter à un écran

Maintenant, connectons nous !

Reprenons notre exemple précédent. Je veux me connecter à la session lié au fichier **20160.utilisateur1**, je vais taper :

    sudo -u utilisateur1 screen -x 20160.utilisateur1

(`sudo` ici est utilisé pour changer d’utilisateur)

Et vous voila dans la fenêtre ! ;)

## Se déconnecter de l’écran

Pour se déconnecter, soit vous terminer la session avec exit, soit vous ne pouvez pas couper la session, et la laisser tourner une fois déconnecté.

Pour se déconnecter il faut utiliser **CTRL+A+D**.


# Cannot open your terminal ‘/dev/pts/0′ – please check.

Je vous redirige vers un précédent article: [Cannot open your terminal ‘/dev/pts/0′ – please check](/cannot-open-your-terminal-‘devpts0′-–-please-check/).